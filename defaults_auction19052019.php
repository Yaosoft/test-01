<?php
function hook_custom_paypal_payment($c)
{
    return apply_filters('hook_custom_paypal_payment', $c);
}

class core_auctions
{
    function _expiry_listing_action($c)
    {
        global $CORE, $post, $wpdb, $userdata;

        // GET LISTING ID
        if (is_numeric($c)) {
            $LISTINGID = $c;
        } elseif (isset($post->ID) && is_numeric($post->ID)) {
            $LISTINGID = $post->ID;
        } else {
            return;
        }

        // WORK OUT ANY OF THE AUCTION DETAILS AND WINNER
        $current_bidding_data = get_post_meta($LISTINGID, 'current_bid_data', true);
        $reserve_price = get_post_meta($LISTINGID, 'price_reserve', true);
        $price_current = get_post_meta($LISTINGID, 'price_current', true);

        // RE-ORDER DATA
        if (!is_array($current_bidding_data)) {
            $current_bidding_data = array();
        }
        krsort($current_bidding_data);

        // CHECK RESERVE PRICE AND INCREASE USERS BID IF LOWER THAN RESERVE
        $checkme = current($current_bidding_data);

        if ($reserve_price != "" && $reserve_price > 0 && $reserve_price > $price_current) {
            if (isset($checkme['username']) && $checkme['max_amount'] >= $reserve_price && is_numeric($reserve_price) && $reserve_price != "0") {
                // update the price so it meets the reserve
                update_post_meta($LISTINGID, 'price_current', get_post_meta($LISTINGID, 'price_reserve', true));

                $price_current = $reserve_price;
            }
        }

        // IF NO RESERVE PRICE CHECK FOR THE HIGHEST BIDDER
        // ??


        // REMOVE EXPIRY DATE
        update_post_meta($LISTINGID, 'listing_expiry_date', '');

        //SEND EMAIL TO BIDDERS
        $_POST['winningbid'] = hook_price($price_current);
        $_POST['title'] = $post->post_title;
        $_POST['link'] = get_permalink($LISTINGID);


        // LOOP BIDDERS
        if (is_array($current_bidding_data) && !empty($current_bidding_data)) {
            $sent_to_array = array();
            $i = 1;
            foreach ($current_bidding_data as $maxbid => $data) {
                if ($i == 1 && $data['max_amount'] > 0) {

                    // CHECK IF THE AUCTION WAS WON SUCCESSULlY
                    if ($reserve_price > 0 && $reserve_price > $price_current) {    //new


                    } else {

                        $_POST['username'] = $data['username'];
                        $CORE->SENDEMAIL($data['userid'], 'auction_ended_winner');

                        update_post_meta($LISTINGID, 'bidstring', '');
                        update_post_meta($LISTINGID, 'bidwinnerstring', get_post_meta($LISTINGID, 'bidwinnerstring', true) . "-" . $data['userid'] . "-");

                    }


                } else {
                    if (!in_array($data['userid'], $sent_to_array)) {
                        $_POST['username'] = $data['username'];
                        $CORE->SENDEMAIL($data['userid'], 'auction_ended');
                        array_push($sent_to_array, $data['userid']);
                    }// end if
                }// end if
                $i++;
            }
        }

        //SEND EMAIL TO AUCTION SELLER
        $author_data = get_userdata($post->post_author);
        $_POST['username'] = $author_data->display_name; 
        $CORE->SENDEMAIL($post->post_author, 'auction_ended_owner');

        // IF THE ITEM SOLD, ADD A COMISSION AMOUNT TO THE USERS ACCOUNT SO THEY HAVE TO PAY THE ADMIN
        $comissionadded = 0;
        $price_current = get_post_meta($LISTINGID, 'price_current', true);
        $reserve_price = get_post_meta($LISTINGID, 'price_reserve', true);
        $price_shipping = get_post_meta($LISTINGID, 'price_shipping', true);


        if ($price_current > 0 && ($price_current > $reserve_price) && (isset($GLOBALS['CORE_THEME']['auction_house_percentage']) && strlen($GLOBALS['CORE_THEME']['auction_house_percentage']) > 0)) {

            if (is_numeric($price_shipping) && $price_shipping > 0) {
                $price_current += $price_shipping;
            }

            // WORK OUT AMOUNT OWED BY THE SELLER
            $AMOUNTOWED = ($GLOBALS['CORE_THEME']['auction_house_percentage'] / 100) * $price_current;
            $AMOUNTOWED = -1 * abs($AMOUNTOWED);

            // CHECK WE HAVENT ALREADY DEDUCTED
            $ALREADY_DEDUCTED = get_user_meta($post->post_author, 'wlt_action_deducted', true);
            if (strpos($ALREADY_DEDUCTED, "*" . $LISTINGID) === false) {

                $user_balance = get_user_meta($post->post_author, 'wlt_usercredit', true);
                if ($user_balance == "") {
                    $user_balance = 0;
                }
                $user_balance = $user_balance + $AMOUNTOWED;
                update_user_meta($post->post_author, 'wlt_usercredit', $user_balance);
                update_user_meta($post->post_author, 'wlt_action_deducted', $ALREADY_DEDUCTED . '*' . $LISTINGID);

            }

            $comissionadded = $AMOUNTOWED;

            // SEND EMAIL TO THE SELLER
            $CORE->SENDEMAIL($post->post_author, 'auction_itemsold');

        }

        // ADD LOG ENTRY
        $CORE->ADDLOG("<a href='" . home_url() . "/?p=" . $LISTINGID . "'>" . $post->post_title . '</a> auction finished. (comission ' . $comissionadded . ')', $LISTINGID, '', 'label-inverse');


        return "stop";// this will stop it going to draft

    }

    /* =============================================================================
		[TIMELEFT] - SHORTCODE
		========================================================================== */
    function shortcode_timeleft($atts, $content = null)
    {
        global $wpdb, $userdata, $CORE, $post, $shortcode_tags;
        $STRING = "";
        $strTxt = "";

        extract(shortcode_atts(array('postid' => "", "layout" => "", "text_before" => "", "text_ended" => "", "key" => "listing_expiry_date"), $atts));

        // SETUP ID FOR CUSTOM DISPLAY
        $milliseconds = str_replace("+", "", round(microtime(true) * 100));
        $milliseconds .= rand(0, 10000);

        // CHECK FOR CUSTOM POST ID
        if ($postid == "") {
            $postid = $post->ID;
        }

        // GET VALUE FROM LISTING
        $expiry_date = get_post_meta($postid, $key, true);

        if ($expiry_date == "" || strlen($expiry_date) < 3) {

            // EXPIRED DISPLAY HERE
            if (defined('IS_MOBILEVIEW')) {
                return "<span class='aetxt'>" . $CORE->_e(array('auction', '3')) . "</span>";
            }
 
            // GET THE LISTING DATA
            $str = "";
            //$expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);
            $expiry_date = get_post_meta($post->ID, 'ket_datedefin', true) ? get_post_meta($post->ID, 'ket_datedefin', true) : get_post_meta($post->ID, 'listing_expiry_date', true);
            //$expiry_date = get_post_meta($post->ID, 'ket_datedefin', true);
            $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
            $reserve_price = get_post_meta($post->ID, 'price_reserve', true);
            $price_current = get_post_meta($post->ID, 'price_current', true);


            if (!is_array($current_bidding_data)) {
                $current_bidding_data = array();
            }
            krsort($current_bidding_data);
            $checkme = current($current_bidding_data);

            // AUCTION HAS ENDED
            if ($expiry_date == "" || strtotime($expiry_date) < strtotime(current_time('mysql'))) {

                if (is_numeric($reserve_price) && $reserve_price != "0" && $price_current < $reserve_price) {

                    $strTxt .= $CORE->_e(array('auction', '1'));

                } elseif (isset($checkme['username'])) {

                    $strTxt .= "" . $checkme['username'] . $CORE->_e(array('auction', '2'));

                }

                if (isset($strTxt) && strlen($strTxt) > 1) {

                    $str .= "<span>" . $strTxt . "</span>";

                }

            }

            return "<div class='ea_finished'><span class='aetxt'>" . $CORE->_e(array('auction', '3')) . "</span> " . $str . "</div>";

        } // END EXPIRY DATE DISPLAY

        // SWITCH LAYOUTS
        switch ($layout) {
            case "1":
                {
                    $layout_code = ",layout: '" . $text_before . " {sn} {sl}, {mn} {ml}, {hn} {hl}, and {dn} {dl}',";
                }
                break;
            case "2":
                {
                    $layout_code = ",compact: true, ";
                }
                break;
            default:
                {
                    $layout_code = "";
                }
                break;
        }
        if (strlen($expiry_date) == 10) {
            $expiry_date = $expiry_date . " 00:00:00";
        }

        // REFRESH PAGE EXTR
        $run_extra = "";
        $run_extrab = "";

        // DISPLAY AFTER FINISHED
        if (isset($GLOBALS['flag-single'])) {

            $run_extra = "location.reload(); jQuery('#auctionbidform').hide();";

        } else {

            $run_extra = "jQuery('#timeleft_" . $postid . $milliseconds . "_wrap').html('<div class=ea_finished><span class=aetxt>" . $CORE->_e(array('auction', '3')) . "</span></div>');";

        }

        // BUILD DISPLAY
		$hazard  = rand(0, 100000); 
        $STRING = "<span id='timeleft_" . $postid . $hazard . "_wrap' class='kmtime_$postid$hazard'><span id='timeleft_" . $postid . $hazard . "' class='kmtime'></span></span>";

        // todo: please remove this after the call
        // $STRING .= "<h1> The expirty is {$expiry_date}</h1>";

        // FORE EXPIRY IF ALREADY EXPIRED
        if (strtotime($expiry_date) < strtotime(current_time('mysql'))) {
            $STRING .= "<script> jQuery(document).ready(function(){ CoreDo('" . str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=validateexpiry&pid=" . $postid . "', 'timeleft_" . $postid . $milliseconds . "'); });</script> ";

        }
// Probleme de fin glissante: on efface la session de fin glissante si l'enchère de cet item est terminé	
$expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);
if (($expiry_date == "" || strtotime($expiry_date) < strtotime(current_time('mysql'))) || isset($SHOWPAYMENTFORM)) {
			if( isset( $_SESSION['fg_'.$post->ID] )){
				unset( $_SESSION['fg_'.$post->ID] );
//				echo "Unset!";
			}
		}
//

// probleme au: le prix courant qui doit devenir la plus grande enchère parmis les au, à moins 15 minutes:
	// calcul du timestamp équivalent à - 15 minutes de la fin:
	// $time_left_fifteen = date('Y-m-d H:i:s', strtotime($expiry_date) - 900);
	// $time_left_oneday  = date('Y-m-d H:i:s', strtotime($expiry_date) - 86400);
	$remaining_time = strtotime($expiry_date) - strtotime(current_time('mysql'));
	$id = $post->ID;
	// initialisation du prix sur le systeme
	//if( (get_post_meta($post->ID, 'price_initial', true) && get_post_meta($post->ID, 'price_initial', true) > get_post_meta($post->ID, 'price_current', true)) ){
	//	$current_price = get_post_meta($post->ID, 'price_initial', true); // current price = initial price if lower
	//}
	//else{
	//	$current_price = get_post_meta($post->ID, 'price_current', true);
	//} 

	$STRING .= "<script> 
	
		var id = $post->ID;
		
//		if( id == '14921' ){
//			console.log( 'dateStr02:' + dateStr02 + '--' );
//			console.log( 'dateStr03:' + dateStr03 );
//		}
		
		jQuery(document).ready(function() {

			var expiry_date;
			var hazar;
			setInterval( mytime, 2000 );
			
			function mytime(){
				
				hazar = Math.floor(Math.random() * 10000);     // returns a random 
				
				// replace previous counter so countdown can load a new chrono ay the same place with the new expiry date from ajax
				// The php hazard is to avoid have the same id when an item is present many time on the page.
				jQuery( 'span.kmtime_$postid$hazard' ).replaceWith('<span class= kmtime_$postid$hazard id=timeleft_$postid' + hazar + '_wrap ><span id=timeleft_$postid$hazard' + hazar + ' class=kmtime></span></span>' );
				
				// Get realtime expiring date with ajax
				function jsonServerResponse(operation, callback, JSOoptionalData) {
				JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
				jQuery.ajax({
					type: 'GET',
					url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=realtimeexpirydate&postid=".$postid."',
					success: function( data ){
					// console.log( 'succes: ' + data.trim() );
					expiry_date = data.trim();
					}
				});	
				}
				jsonServerResponse('get_something_from_server', function(returnedJSO){
					console.log(returnedJSO.Result.Some_data);
				}, 'optional_data');

                var dateStr01   = expiry_date;

				// console.log('Expirty date is $expiry_date, ID is $id');
				var date01 = '';
				if( typeof(dateStr01) != 'undefined'){
					var date01 = new Date( dateStr01.replace(/-/g, '/') );
				}

				// Chrono til the end 
				var res =	jQuery('#timeleft_$postid$hazard' + hazar ).countdown({ timezone: " . get_option('gmt_offset') . ",  until: date01, tickInterval: 60, onExpiry: WLTvalidateexpiry". $postid . $hazard ."" . $layout_code . " });
//console.log( res );
				function WLTvalidateexpiry" . $postid.$hazard . "(){ alert('enchère terminée!'); ". $run_extrab . " setTimeout(function(){ CoreDo('" . str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=validateexpiry&pid=" .$postid . "', 'timeleft_ $postid + hazar +'); " . $run_extra . " }, 2000);  };

				function everyMinuteTick(periods){  
					var totalSeconds = (periods[0]*46656000000) + (periods[1]*777600000) + (periods[2]*12960000) + (periods[3]*216000) + (periods[4]*3600) + (periods[5]*60) + (periods[6]*1);  if (totalSeconds <= 120) { jQuery('#end-of-bid').removeClass('hidden'); }
				}
			} 
		});
				
		</script>";

        return apply_filters('shortcode_timeleft', $STRING);
    }


    function __construct()
    {
        if (is_admin()) {

            // BIDDING HISTORY SIDEBAR
            add_action('admin_menu', array($this, '_custom_metabox'));

            // PAGE SETUP OPTIONS IN THE THEME
            add_action('hook_admin_1_pagesetup', array($this, '_hook_admin_1_pagesetup'));
        }


        // MOBILE FUNCTIONS
        add_action('hook_mobile_content_listing_output', array($this, 'mobilelistingcotent'), 1);
        add_action('hook_mobile_content_output', array($this, 'mobilesearchcontent'), 1);
        add_action('hook_mobile_header', array($this, 'mobile_header'));
        add_action('hook_mobile_footer', array($this, 'mobile_footer'));


        add_action('init', array($this, '_init'));
        add_action('wp_head', array($this, '_wp_head'));

        // RELIST AUCTIONS
        add_action('wp_head', array($this, '_relistactions'));

        // HOOK THE EXPIRY FUNCTION FOR AUCTIONS
        add_action('hook_expiry_listing_action', array($this, '_expiry_listing_action'));

        // HOOK INTO THE EDIT PAGE AND ADD-ON STORE FIELDS
        add_action('hook_fieldlist_0', array($this, '_fields'));

        // ADD IN NEW EMAILS
        add_action('hook_email_list_filter', array($this, '_newemails'));

        // ADD IN NEW MEMBER AREA OPTIONS
        add_action('hook_account_dashboard_before', array($this, '_memberblock'));
        add_action('hook_account_after', array($this, '_paymentform'));

        add_action('hook_account_pagelist', array($this, '_hook_account_pagelist'));
        add_action('hook_account_save', array($this, '_saveaccount'));

        // HOOK ALL CUSTOM QUERIES TO REMOVE FINISHED AUCTIONS FROM DISPLAY
        // add_action('hook_custom_queries',  array($this, '_hook_custom_query' ) );

        // HOOK SUBMISSION PAGE AND ADD IN CORE FIELDS
        add_action('hook_add_fieldlist', array($this, '_hook_customfields'));

        // HOOK LISTING EXPIRY DATE FOR NON-PACKAGE ITEMS
        add_action('hook_add_form_post_save_extra', array($this, '_hook_post_save'));

        // CUSTOM OUTPUT FOR PRICES
        add_action('hook_item_pre_code', array($this, '_hook_item_pre_code'));


        // NEW SHORTCODES
        add_action('hook_admin_2_tags_search', array($this, '_new_tags'));
        add_action('hook_admin_2_tags_listing', array($this, '_new_tags'));
        add_action('hook_admin_2_tags_listing', array($this, '_new_tags1'));


        // REMOVE EDIT BOX IF BIDS ARE THERE
        add_action('hook_single_after', array($this, '_removebox'));

        // REQUIRE PAYPAL
        add_action('hook_add_form_abovebutton', array($this, 'requirepaypal'));
        add_action('hook_tpl_add_field_validation', array($this, '_hook_tpl_add_field_validation'));
        add_action('hook_add_form_post_save_extra', array($this, '_hook_add_form_post_save_extra'));


        // ADD IN SHORTCODES
        add_shortcode('BIDDINGHISTORY', array($this, 'shortcode_biddinghistory'));
        add_shortcode('BIDS', array($this, 'shortcode_bids'));

        add_shortcode('BIDDINGFORM', array($this, 'shortcode_biddingform'));
        add_shortcode('BIDDINGTIMELEFT', array($this, 'shortcode_timeleft'));

        // ADD PAYPAL TO ADMIN AREA EDIT SCREEN
        add_action('show_user_profile', array($this, 'extra_user_profile_fields'));
        add_action('edit_user_profile', array($this, 'extra_user_profile_fields'));
        add_action('personal_options_update', array($this, 'save_extra_user_profile_fields'));
        add_action('edit_user_profile_update', array($this, 'save_extra_user_profile_fields'));

        // REMOVE 0 FROM DISPLAY OF LISTING
        add_action('hook_content_listing', array($this, '_hook_content_listing'));

        add_action('hook_account_dashboard_items', array($this, '_hook_account_dashboard_items'));


        // HOOK IN LANGUAGE TEXT
        add_action('hook_language_array', array($this, '_hook_language_array'));

    }


    function _hook_account_dashboard_items($c)
    {
        global $userdata, $wpdb, $CORE;

// GET MESSAGE COUNT
        $mc = $CORE->MESSAGECOUNT($userdata->user_login);
        if ($mc == "") {
            $mc = 0;
        }

// COUNT ITEMS IM BIDDING ON
        $SQL = "SELECT count(*) AS total FROM " . $wpdb->prefix . "posts 
	INNER JOIN " . $wpdb->prefix . "postmeta AS mt2 ON (" . $wpdb->prefix . "posts.ID = mt2.post_id) 
	WHERE " . $wpdb->prefix . "posts.post_type = '" . THEME_TAXONOMY . "_type' 
	AND ( " . $wpdb->prefix . "posts.post_status = 'publish' ) 
	AND mt2.meta_key = 'bidstring' AND mt2.meta_value LIKE ('%-" . $userdata->ID . "-%')";

        $EXISTINGDATA = get_option('wlt_system_counts');
        if (!is_array($EXISTINGDATA) || current_user_can('edit_user', $userdata->ID)) {
            $EXISTINGDATA = array();
        }

        if (!isset($EXISTINGDATA['bidon']['date']) || (isset($EXISTINGDATA['bidon']['date']) && strtotime($EXISTINGDATA['bidon']['date']) < strtotime(current_time('mysql')))) {

            $result = $wpdb->get_results($SQL);

            $EXISTINGDATA['bidon'] = array("date" => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+10 minutes")), "count" => $result[0]->total);

            update_option("wlt_system_counts", $EXISTINGDATA, true);

            $c2 = $result[0]->total;

        } else {

            $c2 = $EXISTINGDATA['bidon']['count'];

        }


        if ($c2 > 0) {
            $s2 = "red";
        }

        // COUNT WINNING ITEMS
        $SQL = "SELECT count(*) AS total FROM " . $wpdb->prefix . "posts 
	INNER JOIN " . $wpdb->prefix . "postmeta AS mt2 ON (" . $wpdb->prefix . "posts.ID = mt2.post_id) 
	WHERE " . $wpdb->prefix . "posts.post_type = '" . THEME_TAXONOMY . "_type' 
	AND ( " . $wpdb->prefix . "posts.post_status = 'publish' ) 
	AND mt2.meta_key = 'bidwinnerstring' AND mt2.meta_value LIKE ('%-" . $userdata->ID . "-%')";

        $EXISTINGDATA = get_option('wlt_system_counts');
        if (!is_array($EXISTINGDATA) || current_user_can('edit_user', $userdata->ID)) {
            $EXISTINGDATA = array();
        }

        if (!isset($EXISTINGDATA['bidwin']['date']) || (isset($EXISTINGDATA['bidwin']['date']) && strtotime($EXISTINGDATA['bidwin']['date']) < strtotime(current_time('mysql')))) {

            $result = $wpdb->get_results($SQL);

            $EXISTINGDATA['bidwin'] = array("date" => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+10 minutes")), "count" => $result[0]->total);

            update_option("wlt_system_counts", $EXISTINGDATA, true);

            $c3 = $result[0]->total;

        } else {

            $c3 = $EXISTINGDATA['bidwin']['count'];

        }

        ob_start();
        ?>

        <li class="list-group-item col-md-3 col-sm-12 col-xs-12 text-center">
            <a href="javascript:void(0);"
               onclick="jQuery('#MyDetailsBlock').hide();jQuery('#MyMsgBlock').show(); jQuery('#MyFeedback').hide(); jQuery('#MyDashboardBlock').hide();">
                <span><?php echo $mc; ?></span> <?php echo $CORE->_e(array('account', '113')); ?></a>
        </li>

        <li class="list-group-item col-md-3 col-sm-12 col-xs-12 text-center">
            <a href="<?php if (!$userdata->ID) {
                echo wp_login_url('');
            } else {
                echo get_home_url(); ?>/?s=&orderby=bidding<?php } ?>"><span><?php echo $c2; ?></span> <?php echo $CORE->_e(array('auction', '97')); ?>
            </a></li>

        <li class="list-group-item col-md-2 col-sm-12 col-xs-12 text-center">
            <a href="<?php if (!$userdata->ID) {
                echo wp_login_url('');
            } else {
                echo get_home_url(); ?>/?s=&orderby=bidwin<?php } ?>">

                <span><?php echo $c3; ?></span> <?php echo $CORE->_e(array('auction', '98')); ?></a></li>


        <?php
        return ob_get_clean();
    }


    /* =============================================================================
	  ADMIN BIDDING HISTORY
	========================================================================== */

    function _custom_metabox()
    {
        add_meta_box('wlt_auction_bidhistory', "Bidding History", array($this, '_admin_bidhistory'), THEME_TAXONOMY . '_type', 'side', 'high');
    }

    function _admin_bidhistory()
    {
        global $post;

        echo do_shortcode('[BIDDINGHISTORY]');
        echo "<a href='edit.php?post_type=listing_type&resetaction=" . $post->ID . "'>reset auction</a>";
    }


    function _hook_add_form_post_save_extra()
    {
        global $userdata;


        if (isset($_POST['user_paypalemail']) && strlen($_POST['user_paypalemail']) > 1) {

            update_user_meta($userdata->ID, 'user_paypalemail', $_POST['user_paypalemail']);

        }

    }


    /* =============================================================================
	  MOBILE ADJUSTMENTS
	========================================================================== */

    function mobilelistingcotent()
    {
        global $post, $userdata;
        $GLOBALS['CUSTOMMOBILECONTENT'] = true; ?>

        <?php if ($post->post_author == $userdata->ID) { ?><a href="[EDIT]">Edit</a><?php } ?>

        <div style="background:#fff; padding:10px; border:1px solid #ddd;">

            <h1 style="font-size:16px; margin-top:0px; margin-bottom:10px; padding-bottom:10px; border-bottom:1px solid #efefef; text-align:center;">
                [TITLE]</h1>

            <div class="text-center">[IMAGES]</div>

            <ul class="menulist">

                <li class="timeleft">[TIMELEFT]</li>

            </ul>

            [BIDDINGFORM]


            <ul class="nav nav-tabs rida" id="Tabs">

                <li class="active"><a href="#t1" data-toggle="tab">{Description}</a></li>

                <li><a href="#t2" data-toggle="tab">{Details}</a></li>


            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="t1"> [CONTENT] [GOOGLEMAP]</div>

                <div class="tab-pane" id="t2">[FIELDS hide="map"]</div>


            </div>
            <hr
            /
            <div>L'item se trouve à: [LOCATION]</div>
            <hr/>

            <ul class="menulist">

                <li>[FAVS]</li>


            </ul>

        </div>

    <?php }


    function mobilesearchcontent($c)
    {
        global $post, $CORE;
        $GLOBALS['CUSTOMMOBILECONTENT'] = true;

        ?>

        <div style="padding:10px;">

            [IMAGE]

            <h2 style="font-weight:bold;margin-top:0px;">[TITLE]</h2>

            [EXCERPT size=60]

        </div>

        <div class="clearfix"></div>

        <div style="background:#eee; height:30px; padding:5px; font-size:11px; text-transform:uppercase; color:#999; font-weight:bold; ">
            <div class="row">
                <div class="col-md-5 col-xs-6">[TIMELEFT layout=2]</div>
                <div class="col-md-7 col-xs-6 text-right"> [BIDS] <?php echo $CORE->_e(array('auction', '88')); ?> /
                    [hits] <?php echo $CORE->_e(array('single', '19')); ?> </div>
            </div>
        </div>
    <?php }

    function mobile_header()
    { ?>
        <style>
            .nav-pills {
                display: none;
            }

            .countdown_section {
                display: block;
                float: left;
                font-size: 80%;
                text-align: center;
            }

            .single .countdown_holding span {
                color: #888;
            }

            .single .countdown_row {
                clear: both;
                width: 100%;
                padding: 0px 0px;
                text-align: center;
            }

            .single .countdown_show1 .countdown_section {
                width: 98%;
            }

            .single .countdown_show2 .countdown_section {
                width: 48%;
            }

            .single .countdown_show3 .countdown_section {
                width: 32.5%;
            }

            .single .countdown_show4 .countdown_section {
                width: 24.5%;
            }

            .single .countdown_show5 .countdown_section {
                width: 19.5%;
            }

            .single .countdown_show6 .countdown_section {
                width: 16.25%;
            }

            .single .countdown_show7 .countdown_section {
                width: 14%;
            }

            .single .countdown_section {
                display: block;
                float: left;
                font-size: 75%;
                text-align: center;
            }

            .single .countdown_amount {
                font-size: 200%;
            }

            .single .countdown_descr {
                display: block;
                width: 100%;
            }

            .timeleft {
                height: 60px;
            }

            .timeleftbox {
                font-weight: bold;
                color: #666;
            }

            .searchblock .frame img {
                float: left;
                padding-right: 20px;
            }

            .single #auctionbidbox {
                margin-top: 20px;
                margin-bottom: 20px;
            }

            .single #auctionbidbox .topbits div {
                text-align: center;
            }

            .single #auctionbidbox .topbits span {
                font-weight: bold;
                display: block;
                color: gray;
            }

            .single #auctionbidbox .topbits a {
                color: gray;
            }

            .single #auctionbidbox .pricebits div .txt {
                font-weight: bold;
                font-size: 12px;
                line-height: 40px;
            }

            .single #auctionbidbox .pricebits .priceextra {
                font-size: 11px;
                display: block;
                padding-top: 5px;
            }

            .pricebits strong {
                margin-right: 10px;
                line-height: 40px;
            }

            .single #auctionbidbox .bidbox {
                display: none;
            }

            .single #auctionbidbox .bidbox .wrap {
                background: #F7F7F7;
                padding: 20px;
            }

            .single #auctionbidbox .bidbox textarea {
                width: 100%;
                height: 100px;
            }

            .single #auctionbidbox .bidbox .label {
                cursor: pointer;
            }

            .single #auctionbidbox .bidbox .input-group input {
                border-radius: 0px;
                height: 40px;
            }

            .single #auctionbidbox .bidbox .input-group-addon {
                border-radius: 0px;
            }

            .single #auctionbidbox .bidbox .txtb {
                font-size: 18px;
                padding-top: 8px;
            }

            .single #auctionbidbox pre span {
                display: block;
            }

            .single #auctionbidbox .btn {
                font-size: 12px;
                background: #efefef;
                border: 0px;
            }

            .single .paybits .wrap {
                padding: 20px;
            }

            .single .paybits .txt {
                font-weight: bold;
                font-size: 12px;
            }

            .single .paybits .pull-right {
                margin-top: -10px;
            }

            .single .paybits .rnm {
                font-size: 12px;
                font-weight: normal;
            }

            .single .imgbox {
                background: #F7F7F7;
                min-height: 300px;
                padding: 20px;
            }

            .single .imgbox #carousel {
                margin-bottom: 0px !important;
            }

            .single .imgbox .flexslider {
                padding: 0px;
                background: transparent;
            }

            .single #biddinghistory ul {
                max-height: 300px;
                overflow-y: scroll;
            }

        </style>
    <?php }

    function mobile_footer()
    {
        global $post; ?>

    <?php }


    function _hook_account_pagelist($c)
    {

        global $CORE;

        if (isset($GLOBALS['CORE_THEME']['auction_paypal']) && $GLOBALS['CORE_THEME']['auction_paypal'] == '1') {
            $c[] = array(
                "l" => "#top",
                "oc" => "jQuery('#MyDashboardBlock').hide();jQuery('#MyAccountBlock').hide(); jQuery('#MyDetailsBlock').hide(); jQuery('#MyMJobs').show(); jQuery('#MyFeedback').hide(); jQuery('#MyPayments').show();",
                "i" => "glyphicon glyphicon-cog",
                "t" => $CORE->_e(array('auction', '14')),
                "d" => $CORE->_e(array('auction', '26')),
                "e" => "",
            );
        }


        return $c;

    }


    function _hook_content_listing($c)
    {
        global $wpdb, $CORE, $post;

        if (get_post_meta($post->ID, 'auction_type', true) == 2) {

            $c = str_replace("[price_current]", "[price_bin]", $c);
            $c = str_replace('class="bids"', 'class="bids" style="display:none;"', $c);
            $c = str_replace('<span class="bids"', '<span class="bids">' . $CORE->_e(array('auction', '55')) . '</span> <span class="bids"', $c);
        }

        return $c;
    }

    function save_extra_user_profile_fields($user_id)
    {
        global $CORE;
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }

        update_user_meta($user_id, 'user_paypalemail', $_POST['user_paypalemail']);
    }

    function extra_user_profile_fields($user)
    {
        global $wpdb, $CORE; ?>

        <h3>PayPal Information</h3>


        <table class="form-table">


            <tr>
                <th><label>PayPal Email</label></th>
                <td>
                    <input type="text" name="user_paypalemail"
                           value="<?php echo get_user_meta($user->ID, 'user_paypalemail', true); ?>"
                           class="regular-text"/>
                </td>
            </tr>


        </table>

    <?php }


    //1. HOOK INTO THE ADMIN MENU TO CREATE A NEW TAB
    function _hook_admin_1_pagesetup()
    {
        global $wpdb, $CORE;
        $core_admin_values = get_option("core_admin_values"); ?>

        <div class="accordion-group">
            <div class="accordion-heading" style="background:#fff;">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#pagesetup_according"
                   href="#extra1">
                    <h4 style="margin:0xp;font-weight:bold;">
                        <img src="<?php echo get_template_directory_uri(); ?>/framework/admin/img/icons/set.png">
                        Auction Settings <span style="font-size:12px;">(view/hide)</span></h4>
                </a>
            </div>

            <div id="extra1" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="innerwrap content">


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip"
                                   data-original-title="This open will show/hide auctions which are finished from displaying."
                                   data-placement="top">Hide Finished Auctions</label>
                            <div class="controls span6">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="off"
                                                   onchange="document.getElementById('hide_expired').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="on"
                                                   onchange="document.getElementById('hide_expired').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['hide_expired'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="hide_expired" name="admin_values[hide_expired]"
                                   value="<?php echo $core_admin_values['hide_expired']; ?>">
                        </div>


                        <div class="heading2">Auction Terms &amp; Conditions</div>


                        <div class="form-row control-group row-fluid" id="myaccount_page_select">
                            <label class="control-label span5" for="normal-field">House Percentage</label>
                            <div class="controls span6">
                                <div class="input-prepend">
                                    <span class="add-on">%</span>
                                    <input type="text" name="admin_values[auction_house_percentage]"
                                           value="<?php echo stripslashes($core_admin_values['auction_house_percentage']); ?>"
                                           class="span4">
                                </div>
                                <p>This is the % you will keep from sold auctions.</p>
                            </div>
                        </div>

                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span5" rel="tooltip"
                                   data-original-title="this will allow the user to relist their item if unsold."
                                   data-placement="top">Relist Option</label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_relist').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_relist').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_relist'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_relist"
                                   name="admin_values[auction_relist]"
                                   value="<?php echo $core_admin_values['auction_relist']; ?>">
                        </div>


                        <!------------ FIELD -------------->
                        <div class="form-row control-group row-fluid">
                            <label class="control-label">Terms &amp; Conditions</label>
                            <div class="controls">
                                <textarea class="row-fluid" style="height:50px; font-size:11px;"
                                          name="admin_values[auction_terms]"><?php echo stripslashes($core_admin_values['auction_terms']); ?></textarea>
                            </div>
                        </div>


                        <div class="heading2">Auction Listing Settings</div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip"
                                   data-original-title="This open will turn on/off the option for the user to select how long their auction lasts for. Turning this off will default to the listing package length."
                                   data-placement="top">Auction Length Option</label>
                            <div class="controls span6">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="off"
                                                   onchange="document.getElementById('auction_theme_usl').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="on"
                                                   onchange="document.getElementById('auction_theme_usl').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_theme_usl'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_theme_usl"
                                   name="admin_values[auction_theme_usl]"
                                   value="<?php echo $core_admin_values['auction_theme_usl']; ?>">
                        </div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip" data-original-title=""
                                   data-placement="top">Buy Now Option</label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_buynow').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_buynow').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_buynow'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_buynow"
                                   name="admin_values[auction_buynow]"
                                   value="<?php echo $core_admin_values['auction_buynow']; ?>">
                        </div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip" data-original-title=""
                                   data-placement="top">Shipping Option</label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_shipping').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_shipping').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_shipping'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_shipping"
                                   name="admin_values[auction_shipping]"
                                   value="<?php echo $core_admin_values['auction_shipping']; ?>">
                        </div>


                        <div class="heading2">Auction Display Settings</div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip"
                                   data-original-title="Turn ON/OFF the buttons next to the search box in your main header."
                                   data-placement="top">Header Buttons</label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_sbtns').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_sbtns').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_sbtns'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_sbtns" name="admin_values[auction_sbtns]"
                                   value="<?php if (!isset($core_admin_values['auction_sbtns'])) {
                                       echo 1;
                                   } else {
                                       echo $core_admin_values['auction_sbtns'];
                                   } ?>">
                        </div>


                        <div class="heading2">PayPal Settings</div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip"
                                   data-original-title="This will allow users to recieve payments to their PayPal email address."
                                   data-placement="top">Allow User PayPal Payments </label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_paypal').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_paypal').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_paypal'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_paypal"
                                   name="admin_values[auction_paypal]"
                                   value="<?php echo $core_admin_values['auction_paypal']; ?>">
                        </div>


                        <div class="form-row control-group row-fluid ">
                            <label class="control-label span6" rel="tooltip"
                                   data-original-title="Force users to enter their PayPal email before creating new auctions."
                                   data-placement="top">Require PayPal </label>
                            <div class="controls span5">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <label class="radio off">
                                            <input type="radio" name="toggle"
                                                   value="0"
                                                   onChange="document.getElementById('auction_paypal_require').value='0'">
                                        </label>
                                        <label class="radio on">
                                            <input type="radio" name="toggle"
                                                   value="1"
                                                   onChange="document.getElementById('auction_paypal_require').value='1'">
                                        </label>
                                        <div class="toggle <?php if ($core_admin_values['auction_paypal_require'] == '1') { ?>on<?php } ?>">
                                            <div class="yes">ON</div>
                                            <div class="switch"></div>
                                            <div class="no">OFF</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="row-fluid" id="auction_paypal_require"
                                   name="admin_values[auction_paypal_require]"
                                   value="<?php echo $core_admin_values['auction_paypal_require']; ?>">
                        </div>


                    </div>

                </div>
            </div>
        </div>

        <?php

    }


    function _hook_tpl_add_field_validation()
    {
        global $CORE;
        ?>
        var de422    = document.getElementById("user_paypalemail");
        if( de422 != null && de422.value == ''){
        alert('<?php echo $CORE->_e(array('validate', '0')); ?>');
        de422.style.border = 'thin solid red';
        de422.focus();
        colAll();
        return false;
        }
        <?php
    }

    function requirepaypal()
    {
        global $CORE, $userdata;

        if (isset($GLOBALS['CORE_THEME']['auction_paypal_require']) && $GLOBALS['CORE_THEME']['auction_paypal_require'] == '1' && $userdata->ID) {

            $pp = get_user_meta($userdata->ID, 'user_paypalemail', true);
            if ($pp == "") {
                ?>
                <div class="panel panel-default">

                    <div class="panel-heading"><?php echo $CORE->_e(array('auction', '14')); ?></div>

                    <div class="panel-body">

                        <p><?php echo $CORE->_e(array('auction', '16')); ?></p>

                        <hr/>


                        <b><?php echo $CORE->_e(array('auction', '17')); ?> <span class="required">*</span></b>

                        <input type="text" class="form-control" id="user_paypalemail" name="user_paypalemail"
                               value="<?php echo get_user_meta($userdata->ID, 'user_paypalemail', true); ?>">


                        <div class="clearfix"></div>
                        <hr/>

                    </div>
                </div>
                <script>

                    function CheckPD() {

                        if (jQuery("#user_paypalemail").val() == "") {
                            return false;
                        }

                        return true;

                    }


                </script>
                <?php
            }
        }

    }

    function _removebox()
    {
        global $post;

        $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
        if (!is_array($current_bidding_data)) {
        } else {
            echo "<style>#editlistingbox{ display:none;}</style>";
        }
    }


    // ADD A NEW SHROTCODE

    function _new_tags1()
    {
        echo "<br />[ADDBIG] - Displays the 'add-to' Cart Button";
    }


    function _hook_item_pre_code($c)
    {
        global $post;

        $price = get_post_meta($post->ID, 'price_current', true);
        $auction_type = get_post_meta($post->ID, 'auction_type', true);


        $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
        if (!is_array($current_bidding_data)) {
            $bidcount = 0;
        } else {
            $bidcount = count($current_bidding_data);
        }

        // ADD ON PRICE TAG
        $c = str_replace('[price_current]', '<b>' . hook_price($price) . '</b>', $c);

        // ADD ON PRICE TAG
        $c = str_replace('[BIDCOUNT]', $bidcount, $c);

        if ($price == "" || $auction_type == 2) { // || $price == 0

            // ADJUST PRICE
            $bidprice = get_post_meta($post->ID, 'price_bin', true);
            if ($bidprice != "") {
                $c = str_replace("price_current", "price_bin", $c);
            } else {
                $c = str_replace('text="[price_current]"', "text='0&nbsp;'", $c);
            }
        }

        return $c;
    }

    function _hook_post_save($POSTID)
    {

        // SET EXPIRY DATE
        if (is_numeric($packagefields[$_POST['packageID']]['expires']) && !isset($_POST['eid'])) {

        } else {
            // MAKE SURE ITS NOT ALREADY SET
            $g = get_post_meta($POSTID, 'listing_expiry_date', true);
            if ($g == "") {
                update_post_meta($POSTID, 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(current_time('mysql') . " +30 days")));
            }
        }

        // SAVE THE PRICES
        if (is_numeric($_POST['form']['price_bin'])) {
            update_post_meta($POSTID, 'price_bin', $_POST['form']['price_bin']);
        }

        if (is_numeric($_POST['form']['price_reserve'])) {
            update_post_meta($POSTID, 'price_reserve', $_POST['form']['price_reserve']);
        }

    }


    function _hook_customfields($c)
    {
        global $CORE;

        $o = 50;
        $canEditPrice = true;
        if (isset($_GET['eid'])) {

            // CHECK FOR BIDDING SO WE CAN DISABLE FIELDS
            $current_bidding_data = get_post_meta($_GET['eid'], 'current_bid_data', true);
            if (is_array($current_bidding_data) && !empty($current_bidding_data)) {
                $canEditPrice = false;
            }

        }

        if (isset($GLOBALS['CORE_THEME']['auction_buynow']) && $GLOBALS['CORE_THEME']['auction_buynow'] == '1') {

            $c[$o]['title'] = $CORE->_e(array('auction', '4'));
            $c[$o]['name'] = "auction_type";
            $c[$o]['type'] = "select";
            $c[$o]['class'] = "form-control";
            $c[$o]['listvalues'] = array("1" => $CORE->_e(array('auction', '5')), "2" => $CORE->_e(array('auction', '6')));
            $c[$o]['help'] = $CORE->_e(array('auction', '7')) . " <script>
			jQuery('document').ready( function(){
				if(jQuery('#form_auction_type').val() == '2'){ // acheter seulement
					jQuery('#form-row-rapper-price_current').hide(); 
					jQuery('#form-row-rapper-price_reserve').hide();
					jQuery('#form-row-rapper-ket_pourcentageaugmentation').hide(); 
					jQuery('#form-row-rapper-price_regular').show();
					jQuery('#form-row-rapper-price_bin').show();
				} 
				else {                                         // enchere normale 
					jQuery('#form-row-rapper-price_reserve').show();  
					jQuery('#form-row-rapper-price_current').show(); 
					jQuery('#form-row-rapper-price_bin').hide();
					jQuery('#form-row-rapper-price_regular').hide();
					jQuery('#form-row-rapper-ket_pourcentageaugmentation').hide();
				} 
			} );
				
			jQuery('#form_auction_type').change(function(e) { 
				if(jQuery('#form_auction_type').val() == '2'){ // acheter seulement
					jQuery('#form-row-rapper-price_current').hide(); 
					jQuery('#form-row-rapper-price_reserve').hide();
					jQuery('#form-row-rapper-ket_pourcentageaugmentation').hide(); 
					jQuery('#form-row-rapper-price_regular').show();
					jQuery('#form-row-rapper-price_bin').show();
				} 
				else {                                         // enchere normale 
					jQuery('#form-row-rapper-price_reserve').show();  
					jQuery('#form-row-rapper-price_current').show(); 
					jQuery('#form-row-rapper-price_bin').hide();
					jQuery('#form-row-rapper-price_regular').hide();
					jQuery('#form-row-rapper-ket_pourcentageaugmentation').hide();
				} 
			}); </script> ";
            //$c[$o]['required'] 	= true;
            $c[$o]['defaultvalue'] = "0";
        } else {

            $c[$o]['title']  = $CORE->_e(array('auction', '4'));
            $c[$o]['name']   = "auction_type";
            $c[$o]['type']   = "hidden";
            $c[$o]['class']  = "form-control";
            $c[$o]['values'] = 1;
			$type = 1;
        }

        $o++;

		// Prix de depart
        if ($canEditPrice) {
            $c[$o]['title'] = $CORE->_e(array('auction', '74'));
            $c[$o]['name'] = "price_current";
            $c[$o]['type'] = "price";
            $c[$o]['class'] = "form-control";
            $c[$o]['help'] = $CORE->_e(array('auction', '75'));
            $c[$o]['required'] = true;
            $c[$o]['defaultvalue'] = 0;
            $o++;
        }

		// Augmentation minimum
        if ($GLOBALS['CORE_THEME']['auction_theme_usl'] == '1') {
            $c[$o]['title'] = "Augmentation minimum";
            $c[$o]['name'] = "ket_pourcentageaugmentation";
            $c[$o]['type'] = "select";
            $c[$o]['class'] = "form-control";

            $c[$o]['listvalues'] = array
									(
										"5 $"    => "5 $", 
										"10 $"   => "10 $",
										"20 $"   => "20 $",
										"50 $"   => "50 $",
										"100 $"  => "100 $",
										"500 $"  => "500 $",
										"1000 $" => "1000 $",
										"2% du prix actuel $"    => "2% du prix actuel",
										"5% du prix actuel $"    => "5% du prix actuel",
										"10% du prix actuel $"   => "10% du prix actuel",
									);
            $c[$o]['help'] = "Montant ajouté au prix courant pour définir prochain prix de l'enchère.";
            $c[$o]['required'] = true;
            $o++;
        }
		

		// Prix de regulier
        if ($canEditPrice) {
            $c[$o]['title'] = "Prix régulier";
            $c[$o]['name']  = "price_regular";
            $c[$o]['type']  = "price";
            $c[$o]['class'] = "form-control";
            $c[$o]['help']  = "Prix normal que coûte le produit";
            $c[$o]['required']     = false;
            $c[$o]['defaultvalue'] = 0;
            $o++;
        }
		
        if (isset($GLOBALS['CORE_THEME']['auction_buynow']) && $GLOBALS['CORE_THEME']['auction_buynow'] == '1') {
			// Prix acheter maintenant
            if ($canEditPrice) {
                $c[$o]['title'] = $CORE->_e(array('auction', '8'));
                $c[$o]['name']  = "price_bin";
                $c[$o]['type']  = "price";
                $c[$o]['class'] = "form-control";
                $c[$o]['help']  = "Prix promotionnel";
                $c[$o]['required']     = true;
                $c[$o]['defaultvalue'] = 0;
                $o++;
            }
			// Quantité
            $c[$o]['title'] = $CORE->_e(array('auction', '95'));
            $c[$o]['name'] = "qty";
            $c[$o]['type'] = "text";
            $c[$o]['class'] = "form-control";
            $c[$o]['help'] = $CORE->_e(array('auction', '96')) . "<style>#form_qty { width:100px; } </style>";
            $c[$o]['required'] = true;
            $c[$o]['defaultvalue'] = 1;
            $o++;
			// Shipping price
			if ($GLOBALS['CORE_THEME']['auction_theme_usl'] == '1' ) {
				$c[$o]['title'] = "Coût de la livraison";
				$c[$o]['name'] = "price_shipping";
				$c[$o]['type'] = "price";
				$c[$o]['class'] = "form-control";
				$c[$o]['help'] = "Prix de la livraison";
				$c[$o]['required'] = false;
				$c[$o]['defaultvalue'] = 0;
				$o++;
			}
        }
		
		// Prix de reserve
        if ($canEditPrice) {
            $c[$o]['title'] = $CORE->_e(array('auction', '10'));
            $c[$o]['name'] = "price_reserve";
            $c[$o]['type'] = "price";
            $c[$o]['class'] = "form-control";
            $c[$o]['help'] = $CORE->_e(array('auction', '11'));
            $c[$o]['required'] = false;
            $c[$o]['defaultvalue'] = 0;
            $o++;
        }

        if (isset($GLOBALS['CORE_THEME']['auction_shipping']) && $GLOBALS['CORE_THEME']['auction_shipping'] == '1') {
			// shipping price
            if ($canEditPrice) {
                $c[$o]['title'] = $CORE->_e(array('auction', '67'));
                $c[$o]['name'] = "price_shipping";
                $c[$o]['type'] = "price";
                $c[$o]['class'] = "form-control";
                $c[$o]['help'] = $CORE->_e(array('auction', '68'));
                $c[$o]['required'] = true;
                $c[$o]['defaultvalue'] = 0;
                $o++;
            }

        }
		
		// Nombre de jours d'encheres
        if ($GLOBALS['CORE_THEME']['auction_theme_usl'] == '1') {
            $c[$o]['title'] = $CORE->_e(array('auction', '12'));
            $c[$o]['name'] = "listing_expiry_date";
            $c[$o]['type'] = "select";
            $c[$o]['class'] = "form-control";
            $c[$o]['listvalues'] = array("1" => "1 " . $CORE->_e(array('date', '1')), "3" => "3 " . $CORE->_e(array('date', '1')), "5" => "5 " . $CORE->_e(array('date', '1')), "7" => "7 " . $CORE->_e(array('date', '2')), "14" => "14 " . $CORE->_e(array('date', '2')), "21" => "21 " . $CORE->_e(array('date', '2')), "30" => "30 " . $CORE->_e(array('date', '2')));
            $c[$o]['help'] = $CORE->_e(array('auction', '13'));
            $c[$o]['required'] = true;
            $o++;
        }
		
		// Modalités de paiement
        if ($GLOBALS['CORE_THEME']['auction_theme_usl'] == '1') {
            $c[$o]['title']  = "Modalités de paiement";
            $c[$o]['name']   = "ket_modalitédepaiement";
            $c[$o]['type']   = "checkbox";
            $c[$o]['class']  = "form-control";
            $c[$o]['listvalues'] = array
								(
									1 => "Paiement immédiat sur place une fois le lot adjugé pour les enchérisseurs sur place",
									2 => "Paiement en ligne immédiat avec carte de crédit pour les acheteurs en ligne à compter de l’envoi du courriel de confirmation." 
								);
            $c[$o]['help']   = "Avertissement : Tout paiement effectué au nom de MesEnchères Inc. sera acheminé au vendeur (moins les frais de commission du site : entre 6% et 12% HT) avant le retrait du bien par acheteur. En cochant l\\\'une des cases ci-àprès vous acceptez les modalités paiement ainsi que les termes et conditions d\\\'utilisation du site dont nous vous suggèrons fortement de prendre connaissance du contenu.";
            $c[$o]['required'] = false;
            $o++;
        }

		// Modalité de retrait
        if ($GLOBALS['CORE_THEME']['auction_theme_usl'] == '1') {
		    $c[$o]['title'] = "Modalité de retrait";
            $c[$o]['name']  = "ket_modalitéderetrait";
            $c[$o]['type']  = "textarea";
            $c[$o]['class'] = "form-control";
            $c[$o]['help']  = "Comment le produit sera retiré apres l'achat.";
            $c[$o]['required']     = false;
            $c[$o]['defaultvalue'] = "";
            $o++;
		}
		
		
        $c[$o]['title'] = $CORE->_e(array('auction', '91'));
        $c[$o]['name'] = "condition";
        $c[$o]['type'] = "select";
        $c[$o]['class'] = "form-control";
        $c[$o]['listvalues'] = array("1" => $CORE->_e(array('auction', '92')), "2" => $CORE->_e(array('auction', '93')));

        return $c;
    }


    function _hook_custom_query($c)
    {

        $args = array(
            'post_type' => THEME_TAXONOMY . "_type",
            'meta_query' => array(
                array(
                    'key' => 'listing_expiry_date',
                    'value' => '',
                    'compare' => '!='
                ),
            ),
        );

        // ADD-ON USER INPUTTED STRING
        if (!is_array($c)) {

            $bits = explode("&", $c);
            if (is_array($bits)) {
                foreach ($bits as $stringbit) {

                    $v = explode("=", $stringbit);
                    switch ($v[0]) {
                        case "order":
                            {
                                $args = array_merge($args, array("order" => $v[1]));
                            }
                            break;
                        case "orderby":
                            {
                                $args = array_merge($args, array("orderby" => $v[1]));
                            }
                            break;
                    }
                }
            }
        }

        // MERGE VALUES
        $c = array_merge($c, $args);

        return $c;
    }


    function _saveaccount()
    {
        global $CORE, $userdata, $wpdb;

        // SAVE USER CHANGES
        if ($_POST['action'] == "savepaypal" && isset($_POST['user_paypalemail'])) {
            update_user_meta($userdata->ID, 'user_paypalemail', $_POST['user_paypalemail']);
            $GLOBALS['error_message'] = $CORE->_e(array('auction', '15'));
        }
    }

    function _paymentform()
    {
        global $CORE, $post, $wpdb, $userdata;

        if (isset($GLOBALS['CORE_THEME']['auction_paypal']) && $GLOBALS['CORE_THEME']['auction_paypal'] == '1') { ?>
            <!-- START PAYPAL BLOCK   -->
            <div class="panel panel-default" id="MyPayments" style="display:none;">

                <div class="panel-heading"><?php echo $CORE->_e(array('auction', '14')); ?></div>

                <div class="panel-body">

                    <p><?php echo $CORE->_e(array('auction', '16')); ?></p>

                    <hr/>

                    <form method="post">
                        <input type="hidden" name="action" value="savepaypal"/>
                        <b><?php echo $CORE->_e(array('auction', '17')); ?></b>

                        <input type="text" class="form-control" name="user_paypalemail"
                               value="<?php echo get_user_meta($userdata->ID, 'user_paypalemail', true); ?>">


                        <div class="clearfix"></div>
                        <hr/>

                        <div class="text-center">
                            <button class="btn btn-primary btn-lg"
                                    type="submit"><?php echo $CORE->_e(array('button', '6')); ?></button>
                        </div>
                    </form>

                </div>

            </div>
        <?php }
    }

    function _memberblock()
    {
        global $CORE, $post, $wpdb, $userdata; ?>


        <?php

        // GET USER BIDDING DATA
        $user_bidding_data = get_user_meta($userdata->data->ID, 'user_bidding_data', true);

        if (!is_array($user_bidding_data)) {
            $user_bidding_data = array();
        }

        if (!empty($user_bidding_data)) {
            ?>

            <table class="table table-bordered">
                <thead>
                <tr>

                    <th><?php echo $CORE->_e(array('auction', '18')); ?> </th>
                    <th style="width:100px; text-align:center;"><?php echo $CORE->_e(array('auction', '20')); ?></th>
                    <th style="text-align:center;"><?php echo $CORE->_e(array('auction', '21')); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php


                $shownalready = array();

                $user_bidding_data = array_reverse($user_bidding_data, true);
                foreach ($user_bidding_data as $data) {
                    if (!isset($data['postid']) || $data['title'] == "") {
                        continue;
                    }

                    // CHEK ALREADY SHOWN

                    if (!in_array($data['postid'], $shownalready)) {
                        $shownalready[$data['postid']] = $data['postid'];
                    } else {
                        continue;
                    }

                    // CHECK IF LISTING EXISTS OTHERWISE REMOVE IT
                    if (get_post_status($data['postid']) != 'publish' && get_post_type($data['postid']) != THEME_TAXONOMY . "_type") {
                        continue;
                    }

                    // LAST ID TO PREVENT DUPLICATES
                    if (isset($last_id) && $last_id == $data['postid']) {
                        continue;
                    }
                    $last_id = $data['postid'];

                    // GET LINK
                    $link = get_permalink($data['postid']);
                    if ($link == "") {
                        continue;
                    }

                    // GET THE LISTING DATA
                    $expiry_date = get_post_meta($data['postid'], 'listing_expiry_date', true);
                    $current_price = get_post_meta($data['postid'], 'price_current', true);
                    $current_bidding_data = get_post_meta($data['postid'], 'current_bid_data', true);
                    if (!is_array($current_bidding_data)) {
                        $current_bidding_data = array();
                    }
                    krsort($current_bidding_data);
                    $checkme = current($current_bidding_data);


                    if ($expiry_date == "") {
                        $status = $CORE->_e(array('auction', '76'));
                    } else {
                        $status = $CORE->_e(array('auction', '77'));
                    }
                    ?>
                    <tr>
                        <td>

                            <b><a href="<?php echo $link; ?>"><?php echo $data['title']; ?></a></b><br/>
                            <span style="font-size:11px;"><?php echo hook_date($data['date']); ?>
                                | <?php echo $CORE->_e(array('auction', '22')); ?>: <?php echo $status; ?></span>

                            <?php

                            if ($expiry_date == "") {
                                ?>
                                -
                                <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount']; ?>/?delhistory=<?php echo $data['postid']; ?>"
                                   class="label label-success">
                                    <?php echo $CORE->_e(array('checkout', '55')); ?>
                                </a>
                            <?php } ?>


                            <?php if ($expiry_date != "") { ?>

                                <div id='countdown_id<?php echo $data['postid']; ?>'></div>
                                <script>
                                    jQuery(document).ready(function () {

                                        var dateStr = '<?php echo $expiry_date; ?>';
                                        var a = dateStr.split(' ');
                                        var d = a[0].split('-');
                                        var t = a[1].split(':');
                                        var date1 = new Date(d[0], (d[1] - 1), d[2], t[0], t[1], t[2]);

                                        jQuery('#countdown_id<?php echo $data['postid']; ?>').countdown({
                                            until: date1,
                                            timezone: <?php echo get_option('gmt_offset'); ?>,
                                            layout: 'Se termine dans <b>{dn} {dl}, {hn} {hl}, {mn} {ml}, et {sn} {sl}</b>!'
                                        });
                                    });
                                </script>
                            <?php } ?>


                        </td>
                        <td style="text-align:center;"><?php echo hook_price($data['max_amount']); ?> <br/>
                            <small><?php echo $CORE->_e(array('auction', '78')); ?><?php echo hook_price($current_price); ?></small>
                        </td>
                        <td style="text-align:center;">

                            <?php if ($checkme['userid'] == $userdata->ID && get_post_meta($data['postid'], 'auction_price_paid', true) != "" && $CORE->FEEDBACKEXISTS($data['postid'], $userdata->ID) === false) { ?>

                                <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount'] . "/?fdid=" . $data['postid']; ?>"
                                   class="btn btn-success"><?php echo $CORE->_e(array('auction', '23')); ?></a>

                            <?php } else { ?>

                                <a href="<?php echo $link; ?>"
                                   class="btn btn-primary"><?php echo $CORE->_e(array('auction', '24')); ?></a>

                            <?php } ?>

                        </td>
                    </tr>


                    <?php

                    if (isset($has_feedback) && $has_feedback == 1) {
                        ?>
                        <tr>
                            <td colspan="3">

                                <blockquote><?php echo wpautop(get_post_meta($data['postid'], 'feedback_message', true)); ?></blockquote>
                                <small><?php echo $CORE->_e(array('auction', '25')); ?><?php echo hook_date(get_post_meta($data['postid'], 'feedback_date', true)); ?></small>
                                <br/>
                            </td>
                        </tr>
                    <?php } ?>


                <?php } ?>
                </tbody>
            </table>

        <?php } // not empty
        ?>


    <?php }


    function _relistactions()
    {
        global $userdata, $post, $CORE;

        if (isset($GLOBALS['CORE_THEME']['auction_relist']) && $GLOBALS['CORE_THEME']['auction_relist'] == '1' && $post->post_author == $userdata->ID && isset($_GET['relistme']) == 1) {  //&&

            // CHECK THE LISTING HAS EXPIRD
            $expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);

            if ($expiry_date == "" || strtotime($expiry_date) < strtotime(current_time('mysql'))) {


                $days_renew = get_option($post->ID, 'listing_expiry_days', true);

                // GET DAYS FROM THE PACKAGE
                if (!is_numeric($days_renew)) {

                    $packageID = get_post_meta($post->ID, 'packageID', true);
                    $packagefields = get_option("packagefields");
                    if (isset($packagefields[$packageID]['expires']) && is_numeric($packagefields[$packageID]['expires'])) {
                        $days_renew = $packagefields[$packageID]['expires'];
                    }
                }

                if (!is_numeric($days_renew)) {
                    $days_renew = 30;
                }

                // STORE IF FOR PROCESSING
                $SAVEDID = $post->ID;

                // IF NOONE BOUGHT THE ITEM WE CAN RENEW IT OTHERWISE CREATE A NEW LISTING
                if (get_post_meta($post->ID, 'bidwinnerstring', true) != "") {

                    // Gather post data.
                    $my_post = array(
                        'post_title' => $post->post_title,
                        'post_content' => $post->post_content,
                        'post_status' => 'publish',
                        'post_type' => 'listing_type',
                        'post_author' => $post->post_author,
                        'post_category' => array(8, 39)
                    );

                    // Insert the post into the database.
                    $NEWID = wp_insert_post($my_post);

                    // GET CUSTOM FIELDS
                    $custom_fields = get_post_custom($post->ID);
                    foreach ($custom_fields as $key => $value) {
                        update_post_meta($NEWID, $key, $value[0]);
                    }

                    // UPDATE THE OLD POST
                    $my_post1 = array(
                        'ID' => $post->ID,
                        'post_status' => 'trash'
                    );
                    wp_update_post($my_post1);

                    $SAVEDID = $NEWID;


                }

                update_post_meta($SAVEDID, 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(current_time('mysql') . " +" . $days_renew . " days")));
                update_post_meta($SAVEDID, 'current_bid_data', '');
                update_post_meta($SAVEDID, 'bidstring', '');
                update_post_meta($SAVEDID, 'relisted', current_time('mysql'));

                // UPDATE LISTING DATE
                $my_post = array();
                $my_post['ID'] = $SAVEDID;
                $my_post['post_date'] = current_time('mysql');
                wp_update_post($my_post);

                // REDIRECT TO NEW PAGE
                header('location: ' . get_permalink($SAVEDID));
                exit();


            }// end renew
        }

    }

    function _init()
    {
        global $userdata, $post;


        if (current_user_can('edit_user', $userdata->ID)) {

            if (isset($_GET['resetaction']) && is_numeric($_GET['resetaction'])) {

                $days_renew = get_option($_GET['resetaction'], 'listing_expiry_days', true);
                if (!is_numeric($days_renew)) {
                    $days_renew = 30;
                }

                update_post_meta($_GET['resetaction'], 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(current_time('mysql') . " +" . $days_renew . " days")));
                update_post_meta($_GET['resetaction'], 'current_bid_data', '');
                update_post_meta($_GET['resetaction'], 'bidstring', '');
                update_post_meta($_GET['resetaction'], 'listing_price_due', '');

                delete_post_meta($_GET['resetaction'], 'km_hour_bid');
                delete_post_meta($_GET['resetaction'], 'km_minute_bid');
                delete_post_meta($_GET['resetaction'], 'km_current_fixed_bidder');
                delete_post_meta($_GET['resetaction'], 'km_current_auto_bidder');

                // UPDATE LISTING DATE
                $my_post = array();
                $my_post['ID'] = $_GET['resetaction'];
                $my_post['post_date'] = current_time('mysql');
                wp_update_post($my_post);
            }

            if (isset($_GET['resetactionall']) && defined('WLT_DEMOMODE')) {

                // The Query
                $args = array(
                    'post_type' => 'listing_type',

                );
                $the_query = new WP_Query($args);

                // The Loop
                if ($the_query->have_posts()) {

                    foreach ($the_query->posts as $post) {

                        $days_renew = get_option($post->ID, 'listing_expiry_days', true);
                        if (!is_numeric($days_renew)) {
                            $days_renew = 30;
                        }

                        update_post_meta($post->ID, 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(current_time('mysql') . " +" . $days_renew . " days")));
                        update_post_meta($post->ID, 'current_bid_data', '');
                        update_post_meta($post->ID, 'bidstring', '');
                        update_post_meta($post->ID, 'bidwinnerstring', '');

                        // UPDATE LISTING DATE
                        $my_post = array();
                        $my_post['ID'] = $post->ID;
                        $my_post['post_date'] = current_time('mysql');
                        wp_update_post($my_post);

                    }

                }
                /* Restore original Post Data */
                wp_reset_postdata();


            }
        }
    }


    function _wp_head()
    {
        global $CORE, $post, $wpdb, $userdata;
		
		// reset total amount meta for fixed auction
				// 
		delete_post_meta($post->ID, 'tmp_price');
		

        if (isset($_GET['delhistory']) && is_numeric($_GET['delhistory']) && $userdata->ID) {

            $newdata = array();
            $user_bidding_data = get_user_meta($userdata->ID, 'user_bidding_data', true);

            $user_bidding_data = array_reverse($user_bidding_data, true);
            foreach ($user_bidding_data as $key => $data) {

                if (!isset($data['postid']) || $data['title'] == "") {
                    continue;
                }

                if ($_GET['delhistory'] == $data['postid']) {

                } else {

                    $newdata[] = $data;

                }
            }

            update_user_meta($userdata->ID, 'user_bidding_data', $newdata);
        }
		
$current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);

//echo "<pre>";							
//var_dump($current_bidding_data);
//echo "</pre>";

	// UPDATE THE LISTING WITH THE NEW CURRENT PRICE


        if (isset($_POST['auction_action'])) {
//echo "<pre>";							
//var_dump( $_POST );
//echo "</pre>";
//          Probleme de repetition de l'enchère quand on rafraichit la page apres avoir posté le formulaire d'achat
			if( $_POST['kmBidType'] == 2 ){
				if( ! isset( $_SESSION['posted_au'][$userdata->ID][$post->ID] )){
					$acces = true;
					$_SESSION['posted_au'][$userdata->ID][$post->ID] = $_POST['bidamount'];
				}
				else if ( $_SESSION['posted_au'][$userdata->ID][$post->ID] != $_POST['bidamount']){
					$acces = true;
					$_SESSION['posted_au'][$userdata->ID][$post->ID] = $_POST['bidamount'];
				}
				else{
					$acces = false;
				}
			}
			if( $_POST['kmBidType'] == 1 ){
				$oldprice = get_post_meta( $post->ID, 'price_current', true );
				if( $oldprice == $_POST['bidamount'] ){
					$acces = false;
				}
				else{
					$acces = true;
				}
			}
//echo "oldprice: ".$oldprice."<br>";
//var_dump($_SESSION['posted_au'][$userdata->ID][$post->ID]);

			if( $acces ){ // on ne traite le formulaire d'enchère que si l'acces est accordé ci-dessus
			
				// VERIFY THAT THE AUCTION HASNT FINISHED TO STOP EXTRA BIDS
				$expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);
				if ($expiry_date == "" || ( strtotime($expiry_date) < strtotime(current_time('mysql')) )) {

					// LEAVE MSG
					$GLOBALS['error_message'] = $CORE->_e(array('auction', '3'));

					// ADD LOG ENTRY
					$CORE->ADDLOG("{overtime bid stopped} <a href='(ulink)'>" . $userdata->display_name . "</a> on listing <a href='(plink)'>" . $post->post_title . '</a>', $userdata->ID, $post->ID, 'label-warning');

					return;
				}

				switch ( $_POST['auction_action'] ) {
					// BUY NOW OPTION
					case "buynow":
                    {

                        // SET THE CURRENT PRICE TO THE BUYNOW PRICE
                        $bin_price = get_post_meta($post->ID, 'price_bin', true);
                        update_post_meta($post->ID, 'price_current', $bin_price);

                        //3. ADD ON THE NEW BID
                        $current_bidding_data[$bin_price] = array('max_amount' => $bin_price, 'userid' => $userdata->data->ID, 'username' => $userdata->data->user_nicename, 'date' => current_time('mysql'), 'bid_type' => "buynow");

                        //4. UPDATE USER META TO INDICATE THEY BID ON THIS ITEM
                        $user_bidding_data = get_user_meta($userdata->data->ID, 'user_bidding_data', true);
                        if (!is_array($user_bidding_data)) {
                            $user_bidding_data = array();
                        }
                        $user_bidding_data[] = array('postid' => $post->ID, 'max_amount' => $bin_price, 'date' => current_time('mysql'), 'bid_type' => 'bin', 'title' => $post->post_title);
                        update_user_meta($userdata->data->ID, 'user_bidding_data', $user_bidding_data);

                        //4. MERGE THE TWO AND SAVE
                        update_post_meta($post->ID, 'current_bid_data', $current_bidding_data);

                        //5. SEND EMAIL TO BIDDERS
                        $_POST['winningbid'] = hook_price($bin_price);
                        $_POST['title'] = $post->post_title;
                        $_POST['link'] = get_permalink($post->ID);

                        // SEND EMAIL
                        $_POST['username'] = $userdata->display_name;
                        $CORE->SENDEMAIL($userdata->ID, 'auction_ended_winner');


                        // LOOP BIDDERS
                        krsort($current_bidding_data); // order data
                        if (is_array($current_bidding_data) && !empty($current_bidding_data)) {
                            $sent_to_array = array();

                            // SEND EMAIL
                            $_POST['username'] = $userdata->display_name;
                            $CORE->SENDEMAIL($userdata->ID, 'auction_ended_winner');

                            $i = 1;
                            foreach ($current_bidding_data as $maxbid => $data) {

                                if ($i == 1 && $data['max_amount'] > 0) {

                                } else {
                                    if (!in_array($data['userid'], $sent_to_array)) {
                                        $_POST['username'] = $data['username'];
                                        $CORE->SENDEMAIL($data['userid'], 'auction_ended');
                                        array_push($sent_to_array, $data['userid']);
                                    } // end if
                                }// end else
                                $i++;
                            }
                        }

                        //6. SEND EMAIL TO AUCTION SELLER
                        $author_data = get_userdata($post->post_author);
                        $_POST['username'] = $author_data->display_name;
                        $CORE->SENDEMAIL($post->post_author, 'auction_ended_owner');

                        // 7. IF THE ITEM SOLD, ADD A COMISSION AMOUNT TO THE USERS ACCOUNT SO THEY HAVE TO PAY THE ADMIN
                        $comissionadded = 0;
                        $price_current = get_post_meta($post->ID, 'price_current', true);
                        if ($price_current > 0 && isset($GLOBALS['CORE_THEME']['auction_house_percentage']) && strlen($GLOBALS['CORE_THEME']['auction_house_percentage']) > 0) {

                            // WORK OUT AMOUNT OWED BY THE SELLER
                            $AMOUNTOWED = ($GLOBALS['CORE_THEME']['auction_house_percentage'] / 100) * $price_current;
                            $AMOUNTOWED = -1 * abs($AMOUNTOWED);

                            // DEDUCT AMOUNT FROM MEMBERS AREA
                            $user_balance = get_user_meta($post->post_author, 'wlt_usercredit', true);
                            if ($user_balance == "") {
                                $user_balance = 0;
                            }
                            $user_balance = $user_balance + $AMOUNTOWED;
                            update_user_meta($post->post_author, 'wlt_usercredit', $user_balance);

                            $comissionadded = $AMOUNTOWED;

                        }

                        // CHECK FOR QTY ADDED IN 8.2
                        $qty = get_post_meta($post->ID, 'qty', true);
                        if (is_numeric($qty) && $qty > 0) {
                            $qty_sold = get_post_meta($post->ID, 'qty_sold', true);
                            if (!is_numeric($qty_sold)) {
                                $qty_sold = 1;
                            } else {
                                $qty_sold = $qty_sold + 1;
                            }
                            update_post_meta($post->ID, 'qty_sold', $qty_sold);

                            // IF SOLD MORE THAN QTY EXPIRE LISTING
                            if ($qty_sold > $qty) {
                                update_post_meta($post->ID, 'listing_expiry_date', '');
                            }

                            // SET FLAG SO SYSTEM KNOWS WHO THE CURRENT WINNING BIGGER IS
                            update_post_meta($post->ID, 'bidstring', '');
                            update_post_meta($post->ID, 'bidwinnerstring', get_post_meta($post->ID, 'bidwinnerstring', true) . "-" . $userdata->ID . "-");


                        } else {

                            // REMOVE EXPIRY FOR AUTO EXPIRY SO THE CORE SYSTEM DOESNT PICK IT UP
                            // AND THE LISTING IS THEN FINISHED
                            update_post_meta($post->ID, 'listing_expiry_date', '');
                            update_post_meta($post->ID, 'bidstring', '');
                            update_post_meta($post->ID, 'bidwinnerstring', get_post_meta($post->ID, 'bidwinnerstring', true) . "-" . $userdata->ID . "-");

                        }

                        // LEAVE MSG
                        //$GLOBALS['error_message'] = $CORE->_e(array('auction', '28')) . "<style>.timeleftbox { display:none; }</style>";
                        $GLOBALS['error_message'] = "Vous êtes l'acheteur de ce produit !" . "<style>.timeleftbox { display:none; }</style>";

                        // ADD LOG ENTRY
                        $CORE->ADDLOG("<a href='(plink)'>" . $post->post_title . '</a> auction finished. (buy now / comission ' . $comissionadded . ')', $post->ID, '', 'label-inverse');

                        // SEND EMAIL TO THE SELLER
                        $CORE->SENDEMAIL($post->post_author, 'auction_itemsold');

                        // RESET COUNTERS
                        update_option('wlt_system_counts', '');

                    }
                    break;

					// BIDDING AND MAKE OFFER
					case "newbid":
                    {
                        $is_new_bid = false;

                        if (!is_numeric($_POST['bidamount'])) {

                            // LEAVE MSG
                            $GLOBALS['error_message'] = str_replace("%a", $_POST['bidamount'], $CORE->_e(array('auction', '29')));

                        } else {

                            //1. GET ANY CURRENT BIDDING DATA
                            $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
                            
							if (!is_array($current_bidding_data)) {
                                $current_bidding_data = array();
                            }
//echo "<pre>";							
//var_dump($current_bidding_data);
//echo "</pre>";
                            //2. ORDER IT BY KEY (WHICH HOLDS THE BID AMOUNT)
                            krsort($current_bidding_data);

                            // 3. SWITCH THE BID TYPE TO PERFORM ACTIONS

                            $bid_type = $_POST['kmBidType'];
                            switch ($_POST['bidtype']) {


                                // BIDDING SYSTEM
                                case "bid":
                                    {
		
                                        // enchère AUTOMATIQUE: GET THE CURRENT PRICE
// Probleme enchère automatique: quand il ya plusieur enchère automatiques le prix actuel esl celui de l'enchère automatique le plus bas.
										// verifier si il ya des enchère automatique sur ce produit et Attribuer une valeur à $current_price :
										
										// Petite config
										$remaining = strtotime( get_post_meta($post->ID, 'listing_expiry_date', true)) - strtotime( current_time('mysql')); // remaining time
										$posted_amount  = $_POST['bidamount']; // posted price
//echo "posted price: ".$posted_amount."<br>";
										$current_amount = ( get_post_meta($post->ID, 'price_current', true)) ? get_post_meta($post->ID, 'price_current', true) : (get_post_meta($post->ID, 'price_initial', true) ? get_post_meta($post->ID, 'price_initial', true) : 0); // current price or init price or 0 if is the first bid
echo "current price:".$current_amount."<br>";
echo "posted amount:".$posted_amount."<br>";
										$au = $this->array_key_min_and_max( $current_bidding_data, 'max_bid', $current_amount, true); // fonction qui retourne la plus grande et la plus petite des au dans un array. 
										$bidtype    = $_POST['kmBidType'];
										$file_path  = ABSPATH; //get_home_path();
										$reverse    = false;
										$postid     = $post->ID;
echo "bidtype:".$bidtype."<br>";
//echo "remaining: ".$remaining."<br>";
//echo "old au: ";
//var_dump($au);
										// Attribuer une valeur à $current_price en fonction des encheres automatiques placées et de l'enchère posté esur ce produit
										// L'enchère posté est de type enchère automatique ( au ) 
										if( $bidtype == 2 ){ // une au est postée
											if ( isset( $au[0]['amount'] ) ) { // si il existe deja au moin une au sur ce produit
												if( $posted_amount > $au[0]['amount'] && !isset( $au[1]['amount'] ) ){ // si l' au posté est > à l'au courant et > au montant courant
													$current_price = $au[0]['amount']; // automatic bid
echo "Cas 1.0<br>";
												}
												else if( $posted_amount == $au[0]['amount'] && !isset( $au[1]['amount'] ) ){ // si l' au posté est > à l'au courant et > au montant courant
													$current_price = $au[0]['amount']; // le plus petit au devient current_amount
echo "Cas 1.1<br>";
												}
												else  {   
													$current_price = apply_filters('next_bid_price', $current_amount); // automatic bid
echo "Cas 02<br>";
												}
											}
											
											if ( isset( $au[1]['amount'] )){ //  
												if( $posted_amount >= $au[1]['amount'] ){ // si l' au posté est > à l'au courant
													$current_price = $current_amount;
													$late_price = apply_filters('next_bid_price', $au[1]['amount']); // appliqué dans 5 minutes via cron
													$au_amount = $posted_amount; // cecei permettra de voir si les conditions sont toujours réunies
													$old_current_price = $current_price; // peremttra de retrouver les données de cet enchere
													$this->late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path );// une augmentation automatique sera appliquée dans 5 minutes si les conditions restent les memes, au > current_price.
													
echo "Cas 03 <br>";
												}
												if( $posted_amount < $au[1]['amount'] && $posted_amount > $au[0]['amount'] ){
													$current_price = $current_amount;
													$late_price = apply_filters('next_bid_price', $au[0]['amount']); 
													$au_amount = $posted_amount; // cecei permettra de voir si les conditions sont toujours réunies
													$old_current_price = $current_price; // peremttra de retrouver les données de cet enchere
													$this->late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path );// une augmentation automatique sera appliquée dans 5 minutes si les conditions restent les memes, au > current_price.
													
echo "Cas 04<br>";
												}
												if( $posted_amount < $au[0]['amount'] ){
													$reverse = true; // L'enchère automatique sera enregistré comme enchère fixe
													$current_price = $posted_amount; // the lowest
echo "Cas 05 <br>";
												}
											}
											
											if( !isset( $au[0]['amount']) ){ // une toute nouvelle enchère automatique
												$current_price = apply_filters('next_bid_price', $current_amount); // automatic bid
//echo "cas 06 current_amount: ".$current_amount."<br>";
//echo "cas 06 current_price: ".$current_price."<br>";
echo "Cas 06<br>";						
											}
											// if remaining time < 30 minute, current price = highest au
											if( $remaining <=  900  ){ // 15 minutes
												$current_price = $posted_amount; // new au
echo "Cas 07 -- à finaliser<br>";						
											}
										}
										// l'enchère posté est de type fixe. 
										if( $bidtype == 1 ){ // une enchère fixe est postée	
											// si $au[1]['amount'] existe
											if ( isset( $au[1]['amount'] ) && $posted_amount > $au[1]['amount'] ){ // 
												$current_price = $posted_amount; //
echo "Cas 08";
											}
											else if( (isset( $au[1]['amount'] ) && isset($au[0]['amount']) ) && $posted_amount < $au[1]['amount'] && $posted_amount < $au[0]['amount'] ){
												$current_price = $posted_amount;
												$late_price = apply_filters('next_bid_price', $posted_amount); 
												$au_amount = $posted_amount; // cecei permettra de voir si les conditions sont toujours réunies
												$old_current_price = $current_price; // peremttra de retrouver les données de cet enchere
												$this->late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path );// une augmentation automatique sera appliquée dans 5 minutes si les conditions restent les memes, au > current_price.
echo "Cas 09<br>";
											}
											else if ( (isset( $au[1]['amount'] ) && isset($au[0]['amount']) ) && $posted_amount < $au[1]['amount'] && $posted_amount > $au[0]['amount'] ){
												$current_price = $posted_amount;
												$late_price = apply_filters('next_bid_price', $au[0]['amount']); 
												$au_amount = $posted_amount; // cecei permettra de voir si les conditions sont toujours réunies
												$old_current_price = $current_price; // peremttra de retrouver les données de cet enchere
												$this->late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path );// une augmentation automatique sera appliquée dans 5 minutes si les conditions restent les memes, au > current_price.
echo "Cas 10<br>";
											}
											// // si $au[0]['amount'] existe
											if( isset( $au[0]['amount'] ) && $posted_amount > $au[0]['amount'] ){
												$current_price = $posted_amount; //
echo "Cas 11<br>";
											}
											else if( isset( $au[0]['amount'] ) && !isset($au[1]['amount']) && $posted_amount < $au[0]['amount'] ){
												$current_price = $posted_amount;
												$late_price = apply_filters('next_bid_price', $posted_amount ); 
												$au_amount = $posted_amount; // cecei permettra de voir si les conditions sont toujours réunies
												$old_current_price = $current_price; // peremttra de retrouver les données de cet enchere
												$this->late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path );// une augmentation automatique sera appliquée dans 5 minutes si les conditions restent les memes, au > current_price.
echo "Cas 12<br>";
											}
											else if ( !isset( $au[0]['amount'] ) ) {
												$current_price = $posted_amount; //
											}
										}
										//
										
										if( $bid_type == 1 && get_post_meta( $post->ID, 'price_current', true ) <  $posted_amount )
										{
echo "Cas You are now the highest bidder<br>";
											//$is_new_bid = true;
											$checkme = current($current_bidding_data);
											if (isset($checkme['userid']) && $checkme['userid'] != $userdata->data->ID) { 
                                                   $_POST['username'] = $checkme['username'];
                                                   $_POST['title'] = $post->post_title;
                                                   $_POST['link'] = get_permalink($post->ID);
                                                   $CORE->SENDEMAIL($checkme['userid'], 'auction_outbid'); // envoie du message
                                            }
											$GLOBALS['error_message'] = $CORE->_e(array('auction', '31')); // You are now the highest bidder
										}

echo 'new current_price:'.$current_price."<br>"; 
if( $au ){
echo "Old au: ";
var_dump( $au );
echo "<br>"; 
}
else{
	echo "No au <br>"; 
}
										// UPDATE THE LISTING WITH THE NEW CURRENT PRICE
echo "Updated:";
 var_dump( update_post_meta($post->ID, 'price_current', trim($current_price)));
 echo "<br>"; 
 var_dump( get_post_meta($post->ID, 'price_current', true));
 echo "<br>"; 

/**
 code removed here. see backup.
**/
                                    break;
}
                            }

                            //3. ADD ON THE NEW BID
                            $continue_automatic_bid = false;
                            
                                if ($bid_type == 2) { // automatic bid selected
                                    $bid_amount = $_POST['hidden_cp'];  //removed + 1
                                    $km_max_bid = $_POST['bidamount'];

                                    if (get_post_meta($post->ID, 'km_current_auto_bidder', true)) {
                                        preg_match_all('!\d+!', get_post_meta($post->ID, 'km_current_auto_bidder', true), $matches);
                                        $highest_automatic_bidder = $matches[0];
                                    } else {
                                        $highest_automatic_bidder = array(0, 0);
                                    }

                                    if ($highest_automatic_bidder[1] > ($km_max_bid + 1)) {
                                        $GLOBALS['error_message'] = $CORE->_e(array('auction', '30')); // outbid
                                    } else {
                                        if ($highest_automatic_bidder[0] > 0) {
                                            $bid_amount = $highest_automatic_bidder[1];

                                            $email = get_userdata($highest_automatic_bidder[0])->user_email;
                                            $subject = "Vous avez surenchéri";
                                            $message = "Salut, <br /> Vous avez surenchéri sur le produit <a href='" . get_the_permalink($post->ID) . "'>" . get_the_title($post->ID) . "</a>";
                                            $headers = array('Content-Type: text/html; charset=UTF-8');
                                            wp_mail($email, $subject, $message, $headers);
                                            //$CORE->SENDEMAIL($highest_automatic_bidder, 'auction_outbid');
                                            update_post_meta($post->ID, 'km_prev_auto_bidder', $highest_automatic_bidder[0]);
                                        }
                                        $continue_automatic_bid = true;
                                        // do something: todo:
											// Dans un cas 05, l'enchère automatique devient enchère fixe
										if( !$reverse ){ // normal pour une enchère automatique
											$current_bidding_data[$_POST['bidamount']] = array('max_bid' => $_POST['bidamount'], 'max_amount' => $_POST['hidden_cp'], 'userid' => $userdata->data->ID, 'username' => $userdata->data->user_nicename, 'date' => current_time('mysql'), 'bid_type' => $_POST['bidtype']);
										}
										else{            // on l'enregistre comme une enchère fixe
											$current_bidding_data[$_POST['bidamount']] = array('max_amount' => $_POST['hidden_cp'], 'userid' => $userdata->data->ID, 'username' => $userdata->data->user_nicename, 'date' => current_time('mysql'), 'bid_type' => 1);
										}
                                        //4. MERGE THE TWO AND SAVE
                                        update_post_meta($post->ID, 'current_bid_data', $current_bidding_data);

// new au
$au = $this->array_key_min_and_max( $current_bidding_data, 'max_bid', $current_price, true); 
	
echo "Actual_Price: ".$current_price."<br>";
echo "new au: ";
echo "<pre>";
var_dump( $au );
echo "</pre>";
echo "new <br>: ";

                                        // SET FLAG SO SYSTEM KNOWS WHO THE CURRENT WINNING BIGGER IS
                                        update_post_meta($post->ID, 'bidstring', "-" . $userdata->ID . "-");
                                        update_post_meta($post->ID, 'km_current_auto_bidder', $userdata->ID . ':' . $km_max_bid);
										delete_post_meta($post->ID, 'km_current_fixed_bidder');  //to be used in bid_after_automatic_bid()

                                    }


                                } 
								else {
                                    $current_bidding_data[$_POST['bidamount']] = array('max_amount' => $_POST['bidamount'], 'userid' => $userdata->data->ID, 'username' => $userdata->data->user_nicename, 'date' => current_time('mysql'), 'bid_type' => $_POST['bidtype']);

                                    //4. MERGE THE TWO AND SAVE
                                    update_post_meta($post->ID, 'current_bid_data', $current_bidding_data);

                                    // SET FLAG SO SYSTEM KNOWS WHO THE CURRENT WINNING BIGGER IS
                                    update_post_meta($post->ID, 'bidstring', "-" . $userdata->ID . "-");

                                    // UPDATE THE LISTING WITH THE NEW CURRENT PRICE
                                   update_post_meta($post->ID, 'km_current_fixed_bidder', $userdata->ID);  //to be used in bid_after_automatic_bid()

                                }
								// si c'es tun cas 05
								if ( $reverse ){
									$GLOBALS['error_message'] = $CORE->_e(array('auction', '100')); // outbid
								}

                            /*//5. ADD LOG ENTRY
                            $CORE->ADDLOG("<a href='(ulink)'>" . $userdata->user_nicename . '</a> bid on the listing <a href="(plink)"><b>[' . $post->post_title . ']</b></a>.', $userdata->ID, $post->ID, 'label-info');*/

                            // RESET COUNTERS
                            update_option('wlt_system_counts', '');

                            //6. UPDATE USER META TO INDICATE THEY BID ON THIS ITEM
                            $user_bidding_data = get_user_meta($userdata->data->ID, 'user_bidding_data', true);
                            if (!is_array($user_bidding_data)) {
                                $user_bidding_data = array();
                            }
                            if ($bid_type == 2) { // automatic bid selected
                                if ($continue_automatic_bid) {
                                    $bid_amount = $_POST['hidden_cp'] + 1;
                                    $km_max_bid = $_POST['bidamount'];

                                    $user_bidding_data[] = array('postid' => $post->ID, 'max_bid' => $_POST['bidamount'], 'max_amount' => $current_price, 'date' => current_time('mysql'), 'bid_type' => $_POST['bidtype'], 'title' => $post->post_title);
                                    do_action('after_automatic_bid', $_POST['bidamount'], $post->ID, false);

                                    //7. ADD LOG ENTRY
                                    $CORE->ADDLOG("<a href='(ulink)'>" . $userdata->user_nicename . '</a> bid on the listing <a href="(plink)"><b>[' . $post->post_title . ']</b></a>.', $userdata->ID, $post->ID, 'label-info');
                                    update_user_meta($userdata->data->ID, 'user_bidding_data', $user_bidding_data);

                                }
                            } else {
                                $user_bidding_data[] = array('postid' => $post->ID, 'max_amount' => $current_price, 'date' => current_time('mysql'), 'bid_type' => $_POST['bidtype'], 'title' => $post->post_title);

                                //7. ADD LOG ENTRY
                                $CORE->ADDLOG("<a href='(ulink)'>" . $userdata->user_nicename . '</a> bid on the listing <a href="(plink)"><b>[' . $post->post_title . ']</b></a>.', $userdata->ID, $post->ID, 'label-info');
                                update_user_meta($userdata->data->ID, 'user_bidding_data', $user_bidding_data);
                            }

// FIN GLISSANTE

							// petite configuration
				            $max_time_extend = 3; // 10 glissements
							$time_extension  = 60; // 60 secondes (1 minute). Durée d'une extension de temps
							$permission      = 1;
							//
							$item_id = $post->ID;
							$bid = $_POST['bidamount']; // bid amount 
							$current = get_post_meta($post->ID, 'price_current', true); // current price
							$key = "fin_glissante";
							$add_meta = add_post_meta( $post->ID, $key , 0, true );// meta pour coompter les fg
							if ( $remaining <= 120 && $add_meta ) { 
								$permission = 1; // c'est la premiere fois, on accorde la permission de faire une fin glissante
							}
							if( $remaining <= 120 && get_post_meta( $post->ID, $key , true ) > $max_time_extend - 1 ) {		   // the 			
								$permission = 0; // si le nombre max de fin glissant est atteint,
							}
							// pas de fin glissante pour une vente pro
							$c_sticker = get_post_meta($post->ID, 'listing_sticker', true);
							if ( $c_sticker == 11 ) {
								$permission = 0;
							}

							// Fin glissante
// problemme de format de date strtotime: hook_date dans l'argument de strtotime car son formatage n'est pas accepté par strtotime
                            //if ( strtotime( hook_date(get_post_meta($post->ID, 'listing_expiry_date', true))) - (strtotime( current_time('mysql') )) 
							if ( $remaining <= 120 ) { 
								if( $permission == 1 ){
// problemme de format de date strtotime: hook_date dans l'argument de strtotime car son formatage n'est pas accepté par strtotime
									// update_post_meta($post->ID, 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(hook_date(get_post_meta($post->ID, 'listing_expiry_date', true))) + $time_extension));
									
									update_post_meta($post->ID, 'listing_expiry_date', date("Y-m-d H:i:s", strtotime(get_post_meta($post->ID, 'listing_expiry_date', true)) + $time_extension));

									$c_sticker = get_post_meta($post->ID, 'listing_sticker', true);
									$is_user_allowed_to_bid_vente_pro = get_user_meta(get_current_user_id(), 'km_unlock_vente_pro', true);
									//
									$value = get_post_meta( $post->ID, $key , true )  + 1;

									update_post_meta( $post->ID, $key , $value ); // add 1 minute in the session counter
									//
									do_action('bid_extend_vente_pro_expiry', $post->ID);
								}
echo "Fin glissante effectuées: ".get_post_meta( $post->ID, $key , true )."/$max_time_extend" ;
echo "<br>Permission ( 0 ou 1 ) : ".$permission;
							}
                        }
                    }
                    break;

                }// end switch

            } // end if ( acces == true) 
		}// end if isset  $_POST

	}// End bid, function _wp_head()

	/**
	Retourne la valeur maximale et minimales de contenu dans un array recursif. 
	Si une valeur $min est definien on ne condidere que les valeurs de l'array qui sont > à $min.
	**/
	function array_key_min_and_max( $array, $array_key, $min = null ){ // $array is an array of array
		// create an array of all $array_key offset if exist
		// array record sample: 
		// array(26) { 
		//	[24100]=> array(6) {               // automatic auction bid, with his max bid key
		//		["max_bid"]=> string(5) "24100" 
		//		["max_amount"]=> string(5) "23050" 
		//		["userid"]=> string(3) "149" 
		//		["username"]=> string(9) "testor_02" 
		//		["date"]=> string(19) "2019-04-26 11:48:48" 
		//		["bid_type"]=> string(3) "bid" 
		//		} 
		//	[24000]=> array(6) {                // fixed price bid. We don't deal with them
		//		["max_amount"]=> string(5) "24000" 
		//		["userid"]=> string(3) "148" 
		//		["username"]=> string(6) "testor" 
		//		["date"]=> string(19) "2019-04-25 13:27:59" 
		//		["bid_type"]=> string(3) "bid"
		//		}
		//	}

		$array_key_array = array();
		foreach( $array as $value ){
			if ( array_key_exists( $array_key, $value ) ){ // automatic auction bid
				//echo $value['max_bid'].'<br>';
				if( !$min ){
					array_push( $array_key_array, $value[$array_key] );
				}
				else{
					if( $min <= $value[$array_key] ){ 
						array_push( $array_key_array, array ( 
															  'amount'   => $value[$array_key], 
															  'userid'   => $value['userid'], 
															  'username' => $value['username'], 
															));
					}
				}
			}
		}

		// get the higest and the lowest in an array
		$array_key_min_and_max = array();
		if( count( $array_key_array ) ) { 
			sort( $array_key_array ); //reorder

			array_push( $array_key_min_and_max, current( $array_key_array ) );   // push the lowest value
		
			if( count( $array_key_array ) > 1 ){ // is it more than 1 occurence of $array_key ? so we have a lowest and a highest value
				array_push( $array_key_min_and_max, end( $array_key_array ) );   // push the highest value 
			}
		}
//echo "<pre>";							
//var_dump( $array_key_min_and_max );
//echo "</pre>";
		return $array_key_min_and_max;
	}

/**
	Quand le chrono arrive à moins 15 minutes, l'enchère automatic la plus élevée devient le prix (montant courant).
**/
	function fifteen_left( ){
		
		global $CORE, $post;
		
		$current_price = ( get_post_meta($post->ID, 'price_current', true)) ? get_post_meta($post->ID, 'price_current', true) : (get_post_meta($post->ID, 'price_initial', true) ? get_post_meta($post->ID, 'price_initial', true) : 0); // current price or init price or 0 if is the first bid
		
		$current_bidding_data = get_post_meta( $post->ID, 'current_bid_data', true );

		$au = $this->array_key_min_and_max( $current_bidding_data, 'max_bid', $current_price, true); // automatic auction

		if( isset( $au[0]['amount'] ) ){
			$fifteen_price = $au[0]['amount']; 
		}
		
		if( isset( $au[1]['amount'] ) ){
			$fifteen_price = $au[1]['amount']; 
		}
		
		if ( ! isset( $au[1]['amount'] ) &&  isset( $au[0]['amount'] ) ){
			$fifteen_price = $current_price; // current price
		}
		//
		return $fifteen_price; 
	}

/**
	Apres une enchère automatique, quand l'augmentation minumum doit s'appliquer en differé (apres 5 minutes) si les condition restent favorables cad si l'enchère automatique est toujours > au prix courant. On écrit le script dans un fichier. Ce fichier contiendra le script à executer par un cron job configuré à cet effet.
	Cette fonction écrit le script de l'augmentation automatique et l'ajoute dans le fichier executé par le cronjob. 
**/
	function late_bidding( $postid, $au_amount, $old_current_price, $late_price, $file_path  ){ 
		require( "../wp-load.php" );
		$content = "<?php 
						define( 'SHORTINIT', false );
						require( '".$file_path."wp-load.php' );
						global \$post;
						\$res = 0;
						// get the current price and compare it to the automatic bidding amount to see if something have chaged before update
						\$current_price = get_post_meta( $postid, 'price_current', true  );
						\$beforemerge  = '';
						\$bidding_data = '';
						if ( $au_amount > \$current_price ){
							// update current price
							update_post_meta( $postid, 'price_current', $late_price  );
							
							// update post
							\$bidding_data = get_post_meta( $postid, 'current_bid_data', true );// bidding datas
							
							\$current_bidding_data = \$bidding_data[ $old_current_price ];  // get data of this bid
							\$userid    = \$current_bidding_data['userid'];         // userid
							\$username = \$current_bidding_data['username'];	    // username
							
							\$new_current_bidding_data[$late_price] = array
															( 
																'max_amount' => $late_price, 
															    'userid'     => \$userid, 
																'username'   => \$username, 
																'date'       => current_time('mysql'), 
																'bid_type'   => 'bid'
															);
							\$toUpload = array();

							\$merged = \$bidding_data + \$new_current_bidding_data;

                            \$res = update_post_meta( $postid, 'current_bid_data', \$merged );
						}
						
						echo '<pre>';
						var_dump(\$merged);
						echo '</pre>';
						
						echo '<br>';echo '<br>';
						
						var_dump( \$bidding_data );
						echo '<br>';echo '<br>';
						echo 'old_current_price: ".$old_current_price."';
						
						echo '<br>';echo '<br>';
						
						echo '<pre>';
						var_dump(\$a);
						echo '</pre>';
						
						echo '<br>';echo '<br>';
						
						var_dump(\$res);
						var_dump( get_post_meta( $postid, 'current_bid_data', true ) );
				    ?>
					<?php  unlink(__FILE__); // delete this file after execution ?>
					";
					
		// write to file;
		$file = fopen( $file_path."/cronjobs/late_bidding.php", "a" ); 
		fwrite( $file, $content );
		fclose( $file );
	}

/**
	oneday_left_counter	
	returns the next price after applied augmentation minimum, if an au higher than the current price exist
**/
	function oneday_left(){
		
		global $CORE, $post;
		
		$remaining = strtotime( get_post_meta($post->ID, 'listing_expiry_date', true)) - strtotime( current_time('mysql')); 
		$current_price = ( get_post_meta($post->ID, 'price_current', true)) ? get_post_meta($post->ID, 'price_current', true) : (get_post_meta($post->ID, 'price_initial', true) ? get_post_meta($post->ID, 'price_initial', true) : 0); // current price or init price or 0 if is the first bid
		
		$current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
		$au = $this->array_key_min_and_max( $current_bidding_data, 'max_bid', $current_price, true); // automatic auctions
		
		$new_price = apply_filters( 'next_bid_price', $current_price );
		if( isset( $au[0]['amount'] ) && $au[0]['amount'] > $new_price ){
			$res = $new_price;
		}
		else if( isset( $au[1]['amount'] ) && $au[1]['amount'] > $new_price ){
			$res = $new_price;
		}
		else { // prix courant
			$res = $current_price; // no change
		}
		return $res;
	}


	
    // HOOK INTO THE EDIT LISTING PAGE
    function _fields($c)
    {
        global $CORE;

        $list1 = array(

            "tab_action" => array("tab" => true, "title" => "Auction Settings"),

            "auction_type" => array("label" => "Auction Type", "desc" => "", "values" => array("1" => "Normal Auction", "2" => "Classifieds (Buy Now Only)")),
            "price_reserve" => array("label" => "Reserve Price", "desc" => "", "price" => true),
            "price_initial" => array("label" => "Initial Price", "desc" => "This is the initial price of the product.", "price" => true),
            "price_current" => array("label" => "Current Price", "desc" => "This is the current auction price. Only visible in the admin.", "price" => true),
            "price_shipping" => array("label" => "Shipping Price", "desc" => "This is the shipping price for the item. Added to the total after the auction has been won.", "price" => true),
            "price_bin" => array("label" => "Buy Now Price", "desc" => "Leave blank if you do not wish to use this feature.", "price" => true),
            "condition" => array("label" => "Condition", "values" => array("1" => "New", "2" => "Used")),


        );

        return array_merge($c, $list1);
    }


    function _newemails($c)
    {
        global $CORE, $post, $wpdb, $userdata;

        $new_emails = array("n5" => array('break' => 'Auction Emails'),
            "auction_outbid" => array('name' => 'User Outbidded', 'shortcodes' => 'username = (user_login) \n item title = (title) \n link = (link)', 'label' => 'label-warning', 'desc' => 'This email is sent to bidders who are outbid on an auction.'),
            "auction_ended" => array('name' => 'Auction Finished (All Bidders)', 'shortcodes' => 'username = (user_login) \n item title = (title) \n link = (link)\n winning amount = (winningbid)', 'label' => 'label-warning', 'desc' => 'This email is sent to ALL bidders who have bid on an auction which has just finished.'),

            "auction_ended_winner" => array('name' => 'Auction Finished (Winner)', 'shortcodes' => 'username = (user_login) \n item title = (title) \n link = (link)\n winning amount = (winningbid)', 'label' => 'label-warning', 'desc' => 'This email is sent to winner of the auction.'),

            "auction_ended_owner" => array('name' => 'Auction Finished (Seller)', 'shortcodes' => 'username = (username) \n item title = (title) \n link = (link)\n winning amount = (winningbid)', 'label' => 'label-warning', 'desc' => 'This email is sent to auction owners (sellers) to let them know their listing has finished.'),


            "auction_itemsold" => array('name' => 'Auction Finished + Winner (Seller)', 'shortcodes' => 'username = (username) \n item title = (title) \n link = (link)\n winning amount = (winningbid)', 'label' => 'label-warning', 'desc' => 'This email is sent to auction owner (sellers) when their item has been sold.'),


        );

        return array_merge($c, $new_emails);

    }

    function dateToFrench($mydate, $format)
    {
		// echo "mydate: ".$mydate;
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
		$toreturn = str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($mydate))));

		// var_dump( date($format, strtotime($mydate)) );
        return $toreturn;
    }

    /* =============================================================================
BIDDING FORM SHORTCODE
========================================================================== */
    function shortcode_biddingform()
    {
        global $CORE, $post, $userdata, $wpdb;
		$userid =  $userdata->ID;

// GET BASIC BIDDING DATA FOR DISPLAY
        $reserve_price = get_post_meta($post->ID, 'price_reserve', true);
        $price_current = get_post_meta($post->ID, 'price_current', true);
        $price_current = $price_current ? $price_current : get_post_meta($post->ID, 'price_initial', true);
        $price_initial = get_post_meta($post->ID, 'price_initial', true);
        $price_shipping = get_post_meta($post->ID, 'price_shipping', true);
        if ($price_shipping == "" || !is_numeric($price_shipping)) {
            $price_shipping = 0;
        }
        $price_bin = get_post_meta($post->ID, 'price_bin', true);
        $auction_type = get_post_meta($post->ID, 'auction_type', true);
        $condition = get_post_meta($post->ID, 'condition', true);

	//
	?>
	<script>
		// get price with currency
		function goodprice( price, element, operation, callback, JSOoptionalData ) {
				JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
				jQuery.ajax({
					type: 'GET',
					url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=setprice&price='+price+'',
					success: function( data ){
//						console.log( 'succes: ' + data.trim() );
						jQuery( element ).html( data );
					//resultat = data.trim();
					}
				});	

//				if( typeof(resultat) != 'undefined' ){
// 					return resultat;
//				}
		}

		// save price temporary
		function saveprice( qty, price, operation, callback, JSOoptionalData ) {
				JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
				jQuery.ajax({
					type: 'GET',
					url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=saveprice&price='+price+'&id='+postid+'&qty='+qty+'',
					success: function( data ){
//						console.log( 'succes: ' + data.trim() );
					    resultat = data.trim();
						console.log( 'saveprice:' + resultat ) ;
					}
				});	

//				if( typeof(resultat) != 'undefined' ){
					
//				}
		}
		
		
		var total = '';
		// var queue = '<button href=\'#\' id=\'onclick_emul\'>&nbsp;</button>'
		var sa_amount = '';
		var sa_curren = '';
		var price_shipping = "<?php echo $price_shipping ?>";
		var post_title = "<?php echo $post->post_title ?>";
		var userid =  "<?php echo $userid ?>";
		var postid =  "<?php echo $post->ID?>";
		// get CURRENT payment widget with stripe button and paypal button price for fixed bidding auction.
		function gettotal(){
			function jsonServerResponse(operation, callback, JSOoptionalData) {
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
				type: 'GET',
				url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=gettotal&id='+postid+'&shipping='+price_shipping+'&post_title='+post_title+'&userdataid='+userid+'',
				success: function( data ){
					total = data.trim();
					is_sa_already_valide();
					console.log( 'total: ' + total );
//					jQuery('#total_price').html( total );
					// extract total to show before shiping adress validation
					//sa_amount = total.substring(
					//	str.lastIndexOf('name=\"amount\" value=\"') + 1, 
					//	str.lastIndexOf('\"')
					//);
					// extract currency to show before shiping adress validation
					//sa_curren = total.substring(
					//	str.lastIndexOf('name=\"currency_code\" value=\"') + 1, 
					//	str.lastIndexOf('\"')
					//);
					
				}
			});	
			}
			jsonServerResponse('get_something_from_server', function(returnedJSO){	console.log(returnedJSO.Result.Some_data);}, 'optional_data');
//            var totalStr   = total;
//			console.log( 'totalStr: ' + totalStr.trim() );
//			if( typeof(total) != 'undefined' && total != '' ){
//				console.log('total: ' + total);
//				jQuery('#total_price').html( total );
//			}
		}

		// remove x digits in n
		function removeDigits(x, n){ return (x-(x%Math.pow(10, n)))/Math.pow(10, n) } // remove n last digit in x

	</script>;
	<?php
	//
	
// GET HITS
        $hits = get_post_meta($post->ID, 'hits', true);
        if ($hits == "") {
            $hits = 0;
        }

        // GET CURRENT BIDING DATA
        $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
        if (!is_array($current_bidding_data)) {
            $current_bidding_data = array();
        }

        //2. ORDER IT BY KEY (WHICH HOLDS THE BID AMOUNT)
        krsort($current_bidding_data);

        $bid_count = count($current_bidding_data);

        //3. GET THE CURRENT BIDDING DATA
        $checkme = current($current_bidding_data);
        if (isset($checkme['username'])) {

            // SHOW TEXT FOR CURRENT HIGHEST BIDDER
            if ($userdata->data->ID == $checkme['userid']) {

                $current_bidder_amount = "<pre> <i class='fa fa-check-square-o'></i> " . $CORE->_e(array('auction', '33')) . " " . hook_price($checkme['max_amount']) . "";
                // CHECK IF ITS LOWER THAN THE RESERVER PRICE
                if ($price_current != "" && $checkme['max_amount'] >= $reserve_price && is_numeric($reserve_price) && $reserve_price != "0") {

                    $current_bidder_amount .= " " . $CORE->_e(array('auction', '34')) . " " . hook_price($reserve_price) . ". <span>" . $CORE->_e(array('auction', '35')) . "</span>";
                } elseif ($reserve_price > $price_current) {
                    $current_bidder_amount .= " " . $CORE->_e(array('auction', '36')) . " " . hook_price($reserve_price) . ". <span>" . $CORE->_e(array('auction', '37')) . "</span>";
                }

                $current_bidder_amount .= "</pre>";

            }

        } else {
            // DEFAULTS
            $current_bidder_amount = "";

            $bid_count = 0;
        }

        //2. GET THE CURRENT PRICE
        $current_price = get_post_meta($post->ID, 'price_current', true);
        if ($current_price == "") {
            $current_price = 0;
        }
        //<-- add one onto the existing price so the bidder doesnt bid nothing

        //3. GET EXPIRY DATE
        $expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);

        //4. CHECK FOR BIN QTY
        $bin_qty = get_post_meta($post->ID, 'qty', true);

        if (!empty($current_bidding_data)) {
            foreach ($current_bidding_data as $gg) {
                if ($gg['userid'] == $userdata->ID && $gg['bid_type'] == "buynow") {

                    $SHOWPAYMENTFORM = true;
                    // CHECK IFHAS PAID
                    if (strtotime(get_post_meta($post->ID, 'auction_price_paid_date', true)) > strtotime($gg['date'])) {
                        $SHOWPAYMENTFORM = false;
                    }
                }
            }
        }


        // CHECK IF THIS IS AN AFFILIATE PRODUCT OR NOT
        $aff_p = get_post_meta($post->ID, 'buy_link', true);
        $c_sticker = get_post_meta($post->ID, 'listing_sticker', true);
        $is_user_allowed_to_bid_vente_pro = get_user_meta(get_current_user_id(), 'km_unlock_vente_pro', true);
        if ($c_sticker == 11 && !$is_user_allowed_to_bid_vente_pro) {
            if (is_user_logged_in()) {
                $bid_btn = "<a href='" . site_url('autorisation-de-prise-de-caution-vente-speciale') . "' target='_blank'><button class='btn btn-primary form-control btn-lg' style='font-size: 12px;' id='km-bid-authorize'>Demander une autorisation</button></a>";
            }else{
                $bid_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('Veuillez vous connecter pour enchérir.')\">" . $CORE->_e(array('auction', '70')) . "</button>";
            }
        } else {
            if (strlen($aff_p) > 1) {
                $link_l = get_home_url() . "/out/" . $post->ID . "/buy_link/";
                $bid_btn = "<a href='" . $link_l . "' class='btn btn-primary right'>" . $CORE->_e(array('auction', '53')) . "</a>";
            } elseif ($userdata->ID == $post->post_author) { // STOP BIDDING ON OWN ITEMS
                $bid_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('" . $CORE->_e(array('auction', '54', 'flag_noedit')) . "');\">" . $CORE->_e(array('auction', '70')) . "</button>";
            } elseif (!$userdata->ID) {
                $bid_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('" . $CORE->_e(array('auction', '56', 'flag_noedit')) . "');\">" . $CORE->_e(array('auction', '70')) . "</button>";
            } else {
                $bid_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"jQuery('.biddingbox').show();\">" . $CORE->_e(array('auction', '70')) . "</button>";
            }
        }

        // GET BUY NOW BUTTON
        $buynow_btn = "";
        if ($c_sticker == 11 && !$is_user_allowed_to_bid_vente_pro) {
            if (is_user_logged_in()) {
                $buynow_btn = "<a href='" . site_url('autorisation-de-prise-de-caution-vente-speciale') . "' target='_blank'><button class='btn btn-primary form-control btn-lg' style='font-size: 12px;' id='km-bid-authorize'>Demander une autorisation</button></a>";
            }else{
                $bid_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('Veuillez vous connecter pour enchérir.')\">" . $CORE->_e(array('auction', '70')) . "</button>";
            }
        } else {
            if (!$userdata->ID) {
                $buynow_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('" . $CORE->_e(array('auction', '56', 'flag_noedit')) . "');\">" . $CORE->_e(array('auction', '55')) . "</button>";
            } elseif ($userdata->ID == $post->post_author) {
                $buynow_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"alert('" . $CORE->_e(array('auction', '54', 'flag_noedit')) . "');\">" . $CORE->_e(array('auction', '55')) . "</button>";
            } else {
                $buynow_btn = "<button id='km-bid-btn' class='btn btn-primary btn-lg' href='javascript:void(0);' onclick=\"jQuery('.buynowbox').show();\">" . $CORE->_e(array('auction', '55')) . "</button>";
            }
        }

	
// Ajouter un bouton Terminer l'enchère de l'enchère Pro pour l'administrateur et l'auction manager
$role = "";
$c_sticker = get_post_meta( $post->ID, 'listing_sticker',true );
if(  in_array( 'administrator', (array) $userdata->roles ) || in_array('editor', (array) $userdata->roles )){
			// The user has the "administrator" or editor role
			$role = 1;
} 

if( $role == 1 && $c_sticker == 11 ){
	$action = home_url('terminer-lenchere');
	$bid_btn .= "<form action='$action'>
		<input type = 'hidden' name = 'terminer' value = 'terminer' >
		<input type = 'hidden' name = 'postid' value = '$post->ID' >
		<button type='submit' class='btn btn-lg'>Terminer l'enchère</button></form>";
	// $expiry_date = get_post_meta($post->ID, 'listing_expiry_date', true);
}
// fin ajouter un bouton Terminer l'enchère Part 2

        // GET SELLER DETAILS
        $user_info = get_userdata($post->post_author);

        // START OUTPUT
        ob_start();

        ?>

        <?php if ($auction_type == 1) { ?>

        <?php 
		// problemme de format de date strtotime: hook_date dans l'argument de strtotime car son formatage n'est pas accepté par strtotime
		// $end_time = strtotime(hook_date(get_post_meta($post->ID, 'listing_expiry_date', true))); 
		$end_time = strtotime(get_post_meta($post->ID, 'listing_expiry_date', true)); 
		?>
        <?php if ((new DateTime(date("F j, Y g:i:s a", $end_time)))->getTimestamp() > (new DateTime(date("F j, Y g:i:s a")))->getTimestamp()) { ?>
            <div class="row">
                <style>
                    .tooltip-inner {
                        max-width: 350px;
                        /* If max-width does not work, try using width instead */
                        width: 350px;
                        text-align: left;
                        padding: 15px;
                    }

                </style>
                <div class="col-md-6" id="kmsouscaution">
                    <img src="<?php echo get_template_directory_uri() . '/framework/img/image(1).png' ?>" alt=""
                         class="img-responsive" data-toggle="tooltip"
                         title="La caution est une autorisation de prélèvement, si vous êtes déclaré meilleur enchérisseur et que vous respectez les délais de paiement, la caution sera annulée. Si vous n’êtes pas déclaré meilleur enchérisseur la caution sera annulée également. Cette méthode est en vigeur pour assurer la sécurité de nos utilisateurs ainsi que le sérieux et la bonne volonté de tous."
                         style=" border: solid #2e75b5 2px;">
                </div><div class='col-md-6' id='kmfinglissant'>
				
<!-- Fin glissante pour le sticker Vente pro: il ne doit pas s'afficher --> 
<?php $c_sticker = get_post_meta( $post->ID,'listing_sticker',true ); if( $c_sticker != 11 ){echo "<img 
src=".get_template_directory_uri() . "'/framework/img/image.png'  alt='' class='img-responsive' data-toggle='tooltip'title='Durant les 2 dernières minutes avant la fin d’une vente, chaque enchère passée sur le produit, repoussera la fin de la vente de 2 minutes ainsi de suite pour un maximum de 30 minutes.' style=' border: solid #ebb831 2px;'>";}
?>



</div>


            </div>
        <?php }
    }
        ?>
		<!-- image promo, uniquement pour encheres fixes ayant un prix promo -->
		<?php
		if ( $auction_type != 1 && (  get_post_meta($post->ID, 'price_regular', true) ) ){ // 
		?>
		<div class="row">
                <div class="col-md-6" id="kmsouscaution">
                    <img style="border: none" src="<?php echo get_template_directory_uri() . '/framework/class/images/promo_ban.jpg' ?>" 
						 alt=""
                         class="img-responsive" 
						 data-toggle="tooltip"
                         title="Ce produit est en promotion."
                         style=" border: solid #2e75b5 2px;">
                </div>
		</div>
		<?php }
		?>
		
		<!--  -->
        <div class="row">
            <div id="auctionbidform" style="margin-top:10px">

                <ul class="list-group">

                    <li class="list-group-item item1">
                        <span class="pull-right"> <?php echo do_shortcode('[D_SOCIAL size=16]'); ?></span>
                        <?php _e('ID: #', 'premiumpress'); ?><?php echo $post->ID; ?>
                    </li>

                    <li class="list-group-item item1">

                        <?php if ($bid_count > 0) { ?> <i class="fa fa-search bidhistoryicon"></i>

                            <!-----------------------  POPUP ------------------------->
                            <div id="bidhistory" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                x
                                            </button>
                                            <h4 class="modal-title"><?php echo $CORE->_e(array('auction', '18')); ?></h4>
                                        </div>
                                        <div class="modal-body">

                                            <script>

                                                jQuery('.bidhistoryicon').hover(function () {
                                                    jQuery('#bidhistory').modal({
                                                        show: true
                                                    });
                                                });

                                            </script>
                                            <?php echo do_shortcode('[BIDDINGHISTORY]'); ?>


                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!----------------------- end POPUP ------------------------->

                        <?php } ?>

                        <?php echo $CORE->_e(array('auction', '97')); ?>: <strong>
							<span id ="bidcounter">
								<?php echo $bid_count; ?>
							</span>
                        </strong>

                        <?php //if (isset($checkme['username']) && $checkme['username'] != "" && is_numeric($checkme['userid'])) { ?>

                            <span class="pull-right">
		<span id='displayHighestBidder' style=" display: none; ">
			<small><?php echo $CORE->_e(array('auction', '32')); ?></small>:  <i class="fa fa-user"></i> 
				<a href="'#'<?php //echo get_author_posts_url($checkme['userid']); ?>">
					<span id="highestbidder" >
						&nbsp;
					</span>
			</a>

		</span>

                        <?php //} ?>

                    </li>
                    <li class="list-group-item item3">

                        <span class="pull-right"> <i
                                    class="fa fa-line-chart"></i> <?php echo $CORE->_e(array('single', '19')); ?>
                            : <strong><?php echo number_format($hits); ?></strong> </span>

                        <?php echo $CORE->_e(array('auction', '91')); ?>: <strong><?php if ($condition == 1) {
                                echo $CORE->_e(array('auction', '92'));
                            } else {
                                echo $CORE->_e(array('auction', '93'));
                            } ?></strong>
                    </li>

                    <li class="list-group-item item4">

                        <div class="row">

                            <div class="col-md-7">

                                <?php echo $CORE->_e(array('auction', '71')); ?>: <i class="fa fa-user"></i> <strong><a
                                            style="text-decoration:underline;"
	href="<?php echo get_author_posts_url($post->post_author); ?>"><?php echo $user_info->data->display_name; ?></a>
                                </strong>

                            </div>

                            <?php if (isset($GLOBALS['CORE_THEME']['feedback_enable']) && $GLOBALS['CORE_THEME']['feedback_enable'] == '1') { ?>
                                <div class="col-md-5">

                                    <?php echo _user_trustbar($post->post_author, 'inone'); ?>

                                </div>

                            <?php } ?>

                        </div>


                    </li>

                    <!--    <li class="list-group-item item4">-->
                    <!--    --><?php //echo $CORE->_e(array('author','9'));
                    ?><!--: <strong>--><?php //echo hook_date($post->post_date);
                    ?><!--</strong> -->
                    <!--    </li>-->

                    <li class="list-group-item start-of-sales-date">
                        <?php _e("Début de la vente:", 'premiumpress'); ?>
                        <strong>
<!-- problemme de format de date strtotime: hook_date dans l'argument de strtotime car son formatage n'est pas accepté par strtotime -->
						<?php // echo ($start_date = get_post_meta($post->ID, 'ket_datededébut', true)) ? $this->dateToFrench(hook_date($start_date), "j F Y, G:i:s") : $this->dateToFrench(hook_date($post->post_date), "j F Y, G:i:s"); ?>
						<?php echo ($start_date = get_post_meta($post->ID, 'ket_datededébut', true)) ? $this->dateToFrench($start_date, "j F Y, G:i:s") : $this->dateToFrench( $post->post_date, "j F Y, G:i:s"); ?>
<!-- Fin  -->
						</strong>
                    </li>

                    <li class="list-group-item end-of-sales-date">
                        <?php _e("Fin de la vente:", 'premiumpress'); ?>

                        <strong> 
<!-- problemme de format de date strtotime: hook_date dans l'argument de strtotime car son formatage n'est pas accepté par strtotime -->
						<?php //echo ($end_date = get_post_meta($post->ID, 'ket_datedefin', true)) ? $this->dateToFrench(hook_date($end_date), "j F Y, G:i:s") : $this->dateToFrench(hook_date(get_post_meta($post->ID, 'listing_expiry_date', true)), "j F Y, G:i:s"); ?>

						
						<?php echo ($end_date = get_post_meta($post->ID, 'ket_datedefin', true)) ? $this->dateToFrench($end_date, "j F Y, G:i:s") : $this->dateToFrench(get_post_meta($post->ID, 'listing_expiry_date', true), "j F Y, G:i:s"); ?>
<!-- Fin  -->
						</strong>
                    </li>

                    <!--    <li class="list-group-item starting-price">-->
                    <!--        --><?php //_e("Prix de départ:",'premiumpress');
                    ?><!-- <strong>--><?php //echo $tax;
                    ?><!--</strong>-->
                    <!--    </li>-->

                    <li class="list-group-item taxes">
                        <?php _e("Taux de taxes:", 'premiumpress'); ?>
                        <strong><?php echo ($tax = get_post_meta($post->ID, 'ket_tauxdetaxes', true)) ? $tax : __("Inclus dans le prix", 'premiumpress'); ?></strong>
                    </li>

<!-- Initial price: only for direct auction -->
<?php 
if( $auction_type != 1 && (  get_post_meta($post->ID, 'price_regular', true) ) ){
?>
                    <li class="list-group-item prixinit">
                        <?php echo "Prix: "; ?>
                        <strong>
							<span id="iniprice" >
								<?php echo "<span style='color:#6d6d6d; text-decoration:line-through;'>".hook_price( get_post_meta($post->ID, 'price_regular', true) )."</span> "."<span style='color:#da2f0c;'>".hook_price( get_post_meta($post->ID, 'price_bin', true))."</span>" ?> 
							</span>
                        </strong>
                    </li> 
<?php 
}
?>				
<!-- -->

<!-- Economy: only for direct auction -->
<?php 
if( $auction_type != 1 && (  get_post_meta($post->ID, 'price_regular', true) ) ){
$economy = get_post_meta($post->ID, 'price_regular', true) - get_post_meta($post->ID, 'price_bin', true);
?>
                    <li class="list-group-item prixinit">
                        <strong>
							<span id="iniprice">
									<?php echo "Economisez: ".hook_price( $economy ); ?>
							</span>
                        </strong>
                    </li>
<?php 
}
?>				
<!-- -->
                    <li class="list-group-item augmin">
						<?php 
// Probleme remplacer l'augmentation minimum par la quantité pour une enchere fixe
							if ( $auction_type == 1 ){ // Augmentation minimum
								echo _e("Augmentation minimum:", 'premiumpress');
								echo "<strong>".preg_replace("/ /i", " ", get_post_meta($post->ID, 'ket_pourcentageaugmentation', true) ? get_post_meta($post->ID, 'ket_pourcentageaugmentation', true) : __("", 'premiumpress'))."</strong>";
							}
							else{                     // Quantité
								echo "Quantité: ";
								$qty = get_post_meta($post->ID, 'qty', true);
								$def = 1;
								echo "<select style='height: 25px;  width: 55px;' id='quantity' onchange='setTimeout( setPrice(), 2000 );' >";
								foreach( range( 1, $qty ) as $v ){
									echo "<option value = ".$v." ".( $v == $def  ? 'selected' : '' ). " >";
									echo $v;
									echo "</option>";
								}
								echo "</select>";
							}
						?>
					</li>

<!-- calcul du prix quand la quantité change	-->
					<script>
									function setPrice(){
										var priceWithCurrency = "<?php echo hook_price( get_post_meta($post->ID, 'price_bin', true) )   ?>";
										var price_shipping = "<?php  echo trim( $price_shipping ) ?>";
										//var priceWithCurrency = jQuery(" #iniprice" );
										var select   = document.getElementById('quantity'); 
										var qty      = Number( select.options[select.selectedIndex].text );
										var currency =  priceWithCurrency; // price with currency
										var current  = removeDigits( Number(currency.replace(/[^0-9.-]+/g,'')), 2); // price without coma and currency
										var newprice = qty * current;
										
										// save the new price
										saveprice( qty, newprice, 'get_something_from_server', function(returnedJSO){
											console.log(returnedJSO.Result.Some_data);}, 'optional_data' ); //ajax call with a callback function
										
										// Price with delivery for paiement form
										// alert( Number(newprice) + Number(price_shipping) );
										goodprice( ( Number(newprice) + Number(price_shipping) ), '#myModalLabel', 'get_something_from_server', function(returnedJSO){
											console.log(returnedJSO.Result.Some_data);}, 'optional_data' ); //ajax call with a callback function
										
										// Price without shipping price for current price display
										goodprice( newprice, '#buynow_price', 'get_something_from_server', function(returnedJSO){
											console.log(returnedJSO.Result.Some_data);}, 'optional_data' ); //ajax call with a callback function						
									}
							jQuery( document ).ready( setTimeout( setPrice(), 3000 ) ); 
					</script>

                    <?php

                    // CHECK IF THE LISTING HAS ENDED AND THE ITEM HAS BEEN WON
                    if (($expiry_date == "" || strtotime($expiry_date) < strtotime(current_time('mysql'))) || isset($SHOWPAYMENTFORM)) {
						if ($userdata->ID) { ?>
                            <li class="list-group-item paybits text-center">

                                <h4 class="text-center"><?php echo $CORE->_e(array('auction', '39')); ?></h4>
                                <div>

                                    <?php if ($bid_count > 0 && is_numeric($reserve_price) && $reserve_price != "0" && $price_current < $reserve_price) { ?>

                                        <div class="alert alert-danger"><?php echo $CORE->_e(array('auction', '38')); ?></div>

                                    <?php } ?>


                                    <?php echo $this->actions_auctionended(); ?>


                                </div>

                            </li>


                        <?php }
                    } else {
                        // START BIDDING AREA
                        ?>


                        <?php if ($auction_type != 2) { ?>

                            <li class="list-group-item pricebits">

                                <div class="row">

                                    <div class="col-md-6">
                                        <!-- rida -->
                                        <?php echo $CORE->_e(array('auction', '84')); ?>: <strong> <?php

                                            $max = $checkme['max_amount'];
                                            $current = $price_current;
/** Probleme au

                                            if ($max > $current) {

                                                echo hook_price($checkme['max_amount']);
                                            } else {

                                                echo hook_price($price_current);
                                            }
**/
											?>
											<?php 
												$current_price = get_post_meta($post->ID, 'price_current', true);
												$prix = ( $current_price ) ? $current_price : get_post_meta($post->ID, 'price_initial', true);
											?>
											<span id = 'my_current_price'>
											</span>
<!-- real time current price -->
<?php 
$url01 = str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=realtimecurrentprice&postid=$post->ID";

echo
"<script>

	// Get realtime info: current price, current amount, bid counter, best bidder. 
	// This function is called inside a set_intervale
	function realtime_info (){
		
		// Current price
		function realtimeCurrentpriceCallBack(operation, callback, JSOoptionalData) {//  callback function to avoid get Undefined ajax response
		// Get the realtime current price
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
					type: 'GET',
					url: '".$url01."',
					success: function( data ){
						// console.log( 'succes_displayCurrentPrice: ' + data.trim() );
						realtimeCurrentprice_data = data.trim();
					}
			});
		} 
		realtimeCurrentpriceCallBack('get_something_from_server', function(returnedJSO){
			 console.log(returnedJSO.Result.Some_data);
		}, 'optional_data');
		if( typeof( realtimeCurrentprice_data ) != 'undefined' ){
			// Display the  realtime
			var current_price = realtimeCurrentprice_data;
			jQuery('#my_current_price').html( formatMoney( realtimeCurrentprice_data ));
		}
		
		// Bid amount
		function realtimeBidamountCallBack(operation, callback, JSOoptionalData) {//  callback function to avoid get Undefined ajax response
		// Get the realtime current price
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
					type: 'GET',
					url: '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=realtimebidamount&newamount='+current_price+'&postid=".$post->ID."',
					success: function( data ){
						realtimeBidamount_data = data.trim();
					}
			});
		} 
		realtimeBidamountCallBack('get_something_from_server', function(returnedJSO){
			 console.log(returnedJSO.Result.Some_data);
		}, 'optional_data');
		if( typeof( realtimeBidamount_data ) != 'undefined' ){
			// Display the  realtime bidamount
// console.log( realtimeBidamount_data );
// alert( 'current_price: ' + current_price );
			jQuery('#bid_amount').attr( 'placeholder', formatMoney( realtimeBidamount_data ) );
		}
		
		// Bid counter
		function realtimeBidcounterCallBack(operation, callback, JSOoptionalData) {//  callback function to avoid get Undefined ajax response
		// Get the realtime current price
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
					type: 'GET',
					url: '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=realtimebidcount&postid=".$post->ID."',
					success: function( data ){
						realtimeBidcount_data = data.trim();
					}
			});
		} 
		realtimeBidcounterCallBack('get_something_from_server', function(returnedJSO){
			 console.log(returnedJSO.Result.Some_data);
		}, 'optional_data');
		if( typeof( realtimeBidcount_data ) != 'undefined' ){
			// Display 
// console.log( 'realtimeBidcount_data: ' + realtimeBidcount_data );
			jQuery('#bidcounter').html( realtimeBidcount_data ); 
		}

		// Highest user
		function realtimehighestbidderCallBack(operation, callback, JSOoptionalData) {//  callback function to avoid get Undefined ajax response
		// Get the realtime highest bidder
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
					type: 'GET',
					url: '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=realtimehighestbidder&postid=".$post->ID."',
					success: function( data ){
						realtimehighestbidder_data = data.trim();
					}
			});
		} 
		realtimehighestbidderCallBack('get_something_from_server', function(returnedJSO){
			 console.log(returnedJSO.Result.Some_data);
		}, 'optional_data');
		if( typeof( realtimehighestbidder_data ) != 'undefined' ){
			// Display 
 console.log( 'realtimehighestbidder_data: ' + realtimehighestbidder_data );
			if( realtimehighestbidder_data != '' ){
				jQuery('#displayHighestBidder').show(); 
				jQuery('#highestbidder').html( realtimehighestbidder_data ); 
			}
			else{
				jQuery('#displayHighestBidder').hide(); 
			}
		}
	}
	
//	console.log( 'displayCurrentPrice = ' + realtime_price );

	function formatMoney(n, c, d, t) {
		var c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? '.' : d,
		t = t == undefined ? ',' : t,
		s = n < 0 ? '-' : '',
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;

		return s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
	};

</script>";					
?>

<!-- À 23h 59 min 59 sec de la fin d’une vente il ya une enchère automatique, à -15 min de la fin, Disable automatic bid option when -->
<?php
$remaining = strtotime( get_post_meta($post->ID, 'listing_expiry_date', true)) - strtotime( current_time('mysql')); 
echo "
<script>

var myres02;
var fifteen_left_controller = 1;
var oneday_left_controller  = 1;

myres02 = setInterval( scheduled_operations, 2000 );
var update_result_data01;

function scheduled_operations(){
	
	// realtime price
	realtime_info (); // currrent price, bidding price, bid count, best bidder
	
	// The chrono.
	//Get item remaining time with an ajax call to a custom function
	function jsonServerResponse01(operation, callback, JSOoptionalData) {
		JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
		jQuery.ajax({
			type: 'GET',
			url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=remaining_time&postid=".$post->ID."',
			success: function( data ){
				// console.log( 'succes: ' + data.trim() );
				remaining_time_data = data.trim();
			}
		});	
	}
	jsonServerResponse01('get_something_from_server', function(returnedJSO){console.log(returnedJSO.Result.Some_data);}, 'optional_data');
	if( typeof( remaining_time_data ) != 'undefined' ){
		var remaining_time = remaining_time_data;
	}
	console.log( 'remaining_time ' + remaining_time ) ;


	if( remaining_time <= 0 ){ // 0 minutes
		stop_setInterval();
		console.log('Enchere terminée!');
	}
	else if( remaining_time <= 900 ){ // de 15 minutes
//		alert( 'fifteen_left_controller' + fifteen_left_controller );
		if( fifteen_left_controller == 1 ){   // first time
			var newprice01 = '" . $this->fifteen_left() ."'; // ajout de l'augmentation minimal
//			alert( newprice01 );

			// we prefere update via ajax to avoid unwand php execution in this file on page open
			function jsonServerResponse(operation, callback, JSOoptionalData) {
				JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
				jQuery.ajax({
					type: 'GET',
					url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=update_current_price&key=fifteen_left&newprice='+newprice01+'&postid=".$post->ID."',
					success: function( data ){
					// console.log( 'succes: ' + data.trim() );
						update_result_data01 = data.trim();
					}
				});	
			}
			jsonServerResponse('get_something_from_server', function(returnedJSO){console.log(returnedJSO.Result.Some_data);}, 'optional_data');
			if( typeof( update_result_data01 ) != 'undefined' ){
				// price to show
				if( update_result_data01 == 1 ){
					var price = newprice01;
				}
				else{
					var price = ".$current_price.";
				}
					
// alert( 'Moins 15 minutes restante! Prix actuel: ' + price + '. Note: Il n\'ya plus d\'option d\'enchère automatique possible sur cette vente.' );
				fifteen_left_controller = 0; // done at least once
				jQuery('.km_bid_02').prop('disabled',true); // disable automatic auction option radio button
			}
		}
	}
	else if( remaining_time > 900 && remaining_time <= 86400 ){ // moins 24 heure
//		alert( 'oneday_left_controller: ' + oneday_left_controller );
		if( oneday_left_controller == 1 ){   // This is the first time. And the last time
			var newprice02 = '" . $this->oneday_left() ."'; // ajout de l'augmentation minimal
// console.log( 'newprice02' + newprice02 );
			// we prefere update via ajax to avoid unwand php execution in this file on page open
			function jsonServerResponse(operation, callback, JSOoptionalData) {
				JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
				jQuery.ajax({
					type: 'GET',
					url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=update_current_price&key=oneday_left&newprice='+newprice02+'&postid=".$post->ID."',
					success: function( data ){
						// console.log( 'succes: ' + data.trim() );
						update_result_data01 = data.trim();
// alert( data );
						// price to show
						if( update_result_data01 == 1 ){
							var price02 = newprice02;
						}
						else{
							var price02 = ".$current_price.";
						}
// alert( 'Cet enchère expire dans moins de 24h! Prix actuel: ' + price02 );
						oneday_left_controller = 0;
					}
				});	
			}
			jsonServerResponse('get_something_from_server', function(returnedJSO){console.log(returnedJSO.Result.Some_data);}, 'optional_data');
		}
//		else {
//			alert( 'already updated' );
//		}

		// enabling automatic auction button
		if( remaining_time > 900 ){ // de 15 minutes
			jQuery('.km_bid_02').removeAttr('disabled', false); // disable automatic auction option radio button
			jQuery('.km_bid_02').prop('disabled', false); // disable automatic auction option radio button
		}
	}
}

// 	Clear interval
function stop_setInterval(){
	clearInterval( myres02 );
}
</script>";
?>

<!-- end automatic bid option --> 

	
<?php
// À 23h 59 min 59 sec de la fin d’une vente, le système ajoute le montant de l’augmentation minimum au prix actuel du produit, quand dans le système il y a une enchère automatique. 
// calcul du temps restant

//	$remaining = strtotime( get_post_meta($post->ID, 'listing_expiry_date', true)) - strtotime( current_time('mysql')); 
//	if( $remaining < 86400 ){ // il reste une journée. On ajoute l'augmentation minimale 
// var_dump( add_post_meta( $post->ID, 'oneday_left_controller', 'ok', true )  );
//		if( ! add_post_meta( $post->ID, 'oneday_left_controller', 'ok', true ) ){ // ce post n' a pas encore subit l'augmentation de - 24h, on le fait
		//echo '1 day remaining';
			// calcul de l'augmentation minimale.
//			$current_price = ( get_post_meta($post->ID, 'price_current', true)) ? get_post_meta($post->ID, 'price_current', true) : (get_post_meta($post->ID, 'price_initial', true) ? get_post_meta($post->ID, 'price_initial', true) : 0); // current price or init price or 0 if is the first bid
//			$current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true); // current biding data array
//			$au = $this->array_key_min_and_max( $current_bidding_data, 'max_bid', $current_price);    // get lowest and hihest au in an array
//			if( isset( $au[0]['amount'] ) ){ // il ya une au
//				$new_price = apply_filters('next_bid_price', $current_price);
//				var_dump( update_post_meta( $post->ID, 'oneday_left_controller', 'ok' ));
				//update_post_meta( $post->ID, 'price_current',  $new_price); // update the price
//			}
//		}
//	}
?>
                                        </strong>

                                        <div class="clearfix"></div>

                                        <?php if (is_numeric($price_shipping) && $price_shipping > 0) { ?>
                                            <small>
                                                <i class="fa fa-codepen"></i> <?php echo $CORE->_e(array('auction', '67')); ?>
                                                : <?php echo hook_price($price_shipping); ?> </small>
                                        <?php } ?>

                                        <?php
                                        if (strlen($reserve_price) > 0 && $reserve_price != "0" && $price_bin < 1) {
                                            if (is_numeric($reserve_price) && $reserve_price != "0" && $price_current < $reserve_price) {
                                                ?>

                                                <span class="priceextra"><i
                                                            class="fa fa-times"></i> <?php echo $CORE->_e(array('auction', '57')); ?></span>

                                            <?php } else { ?>

                                                <span class="priceextra"><i
                                                            class="fa fa-check"></i> <?php echo $CORE->_e(array('auction', '69')); ?></span>

                                            <?php }
                                        } ?>

                                    </div>

                                    <div class="col-md-6 text-right">

                                        <?php echo $bid_btn; ?>

                                    </div>

                                </div>

                            </li>


                            <?php

                            // RESEVR PRICE NOT YET MET

                            if (strlen($current_bidder_amount) > 4) { ?>
                                <li class="list-group-item">

                                    <?php echo $current_bidder_amount; ?>
                                </li>
                            <?php } ?>


                            <li class="list-group-item biddingbox" style="display:none;">

                                <span class="label label-default pull-right"
                                      onclick="jQuery('.biddingbox').hide();"><?php echo $CORE->_e(array('account', '48')); ?></span>


                                <h5>AUTORISATION DE CAUTION</h5>

                                <textarea readonly
                                          style="margin: 0px; height: 345px; width: 354px;"><?php echo stripslashes($GLOBALS['CORE_THEME']['auction_terms']); ?></textarea>


                                <!--        <form method="post" action="" class="row clearfix" onsubmit="return CheckBidding();">-->

                                <form id="bid-form" method="post" action="" class="row clearfix article-form">
                                    <input type="hidden" name="auction_action" value="newbid"/>
                                    <input type="hidden" name="bidtype" value="bid"/>
									<input type="hidden" id="fifteen_left_counter" value=""/>
									<input type="hidden" id="oneday_left_counter" value=""/>
                                    <input type="hidden" name="hidden_cp" id="hidden_cp" value="<?php
// problem $price_current: on a deja appliqué l'augmentation minimale dans _wp_init(), donc on n'applique plus next_bid_price
                                    //if ($max > $current) {
                                        // echo $checkme['max_amount']+24;
                                    //    echo apply_filters('next_bid_price', $checkme['max_amount']);
                                    //} //else {
                                        //echo $price_current+24;
					
                                        // echo apply_filters('next_bid_price', $price_current);
										 echo  $price_current;
                                    //}
                                    ?>"/>

                                    <div class="col-md-12">
                                        <h4><?php echo $CORE->_e(array('auction', '89')); ?></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-lg" id="bid_amount"
                                                   name="bidamount" placeholder='<?php

//                                          if ($max > $current) {
                                                // echo hook_price($checkme['max_amount']+25);
//                                                echo hook_price(apply_filters('next_bid_price', $checkme['max_amount']));
//                                            } else {
                                                //echo hook_price($price_current+25);
                                            echo hook_price(apply_filters('next_bid_price', $price_current)); 
 //                                           }
                                            ?>'
                                            />
                                            <span class='input-group-addon'><?php echo $GLOBALS['CORE_THEME']['currency']['symbol']; ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <button id="bid-btn" class="btn btn-lg btn-primary"
                                                type="submit"><?php echo $CORE->_e(array('auction', '66')); ?></button>
                                    </div>
                                    <div class="col-md-6" id = "radio_box">
                                        <input type="radio" class = "km_bid_01"  name="kmBidType" id="km_bid_fixed" value="1"  checked>
                                        Enchère Fixe
                                        <br> <input type="radio" class = "km_bid_02" name="kmBidType" id="km_bid_auto" value="2"> Enchère
                                        Automatique 
										<img align="right" src="<?php echo get_template_directory_uri() . '/framework/img/help.png' ?>" alt=""
                         class="img-responsive" data-toggle="tooltip"
                         title="L’enchère automatique permet de fixer un montant maximum d’enchère sur un produit. Le systeme se chargera de suivre le déroulement de la vente à votre place et enchérira pour vous, suivant l'augmentation minimum fixée sur le produit. A 15 minutes de la fin d'une enchère, cette option n'est plus disponible." style="border: none; width: 20px; ">
												
                                    </div>

                                    <input type="hidden" id="km_initial_price" value="<?php echo $price_initial ?>">
                                    <input type="hidden" id="km_item_id" value="<?php echo $post->ID ?>">
                                    <hr/>

                                    <div class="col-md-12" style="margin:10px;">
                                        <div class="checkbox" style="padding-left:10px;">
                                            <input type="checkbox" id="agreeTC" name="interests" class="radio terms"
                                                   tabindex="8">J'ai lu et j'accepte <a
                                                    href="<?php echo $GLOBALS['CORE_THEME']['links']['terms']; ?>"
                                                    style="color:blue; text-decoration:underline;" target="_blank">les
                                                termes et conditions</a>
                                        </div>
                                    </div>

                                </form>
<!--  This form is procecced by the file wp-content/plugins/bid/bid.js bid.js-->
                                <?php do_action('after_bidding_form'); ?>

                                <!-- <h4><?php
                                //  echo $CORE->_e(array('auction','20'));
                                ?></h4> -->


                                <?php
                                // echo $CORE->_e(array('auction','90'));
                                ?>

                                <?php if (strlen($GLOBALS['CORE_THEME']['auction_terms']) > 1) { ?>


                                    <p><?php
                                        // echo $CORE->_e(array('auction','65'));
                                        ?></p>


                                    <!-- START TERMS BOX -->


                                    <script type="text/javascript"> jQuery(document).ready(function () {

                                            jQuery('#core_middle_column form .btn-primary').attr('disabled', true);
                                        });

                                        function UnDMe() {

                                            if (jQuery('#core_middle_column form .btn-primary').is(':disabled') === false) {

                                                jQuery('#core_middle_column form .btn-primary').attr('disabled', true);
                                                jQuery('#core_middle_column form .btn-primary').removeClass('green');
                                                jQuery('#core_middle_column form .btn-primary').addClass('gray');

                                            } else {

                                                jQuery('#core_middle_column form .btn-primary').attr('disabled', false);
                                                jQuery('#core_middle_column form .btn-primary').addClass('green');

                                            }
                                        }
                                    </script>


                                    <!-- END TERMS BOX -->

                                <?php } ?>

                            </li>

                        <?php } else { ?>


                            <?php //if ($price_bin != "" && is_numeric($price_bin) && $price_bin > 0 && ($price_current <= $price_bin)) { ?>


                            <li class="list-group-item pricebits">


                                <div class="row">

                                    <div class="col-md-6">

                                        <?php echo $CORE->_e(array('auction', '8')); ?>:
                                        <strong>
											<span id="buynow_price">
												<?php echo hook_price($price_bin); ?>
											</span>
										</strong>

                                        <?php
                                        // SHOW SHIPPING IF CLASSIFIEDS IT ON
                                        if ($auction_type == 2) { ?>
                                            <?php if (is_numeric($price_shipping) && $price_shipping > 0) { ?>
                                                <span class="priceextra"> <i
                                                            class="fa fa-codepen"></i> <?php echo $CORE->_e(array('auction', '67')); ?>
                                                    : <?php echo hook_price($price_shipping); ?></span>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php

                                        if (is_numeric($bin_qty) && $bin_qty > 1) {
                                            $bin_qty_sold = get_post_meta($post->ID, 'qty_sold', true);
                                            if ($bin_qty_sold == "") {
                                                $bin_qty_sold = 0;
                                            }
                                            ?>
                                            <br/> <?php echo $bin_qty - $bin_qty_sold; ?>/<?php echo $bin_qty; ?> <?php echo $CORE->_e(array('auction', '94')); ?>.
                                        <?php } ?>

                                    </div>


                                    <div class="col-md-6 text-right">

                                        <?php echo $buynow_btn; ?>
                                    </div>

                                </div>


                            </li>

                            <li class="list-group-item buynowbox" style="display:none;">

                                <span class="label label-default pull-right"
                                      onclick="jQuery('.buynowbox').hide();"><?php echo $CORE->_e(array('account', '48')); ?></span>


                                <h4><?php _e("Acheter Maintenant", 'premiumpress') ?></h4>

                                <?php if (strlen($GLOBALS['CORE_THEME']['auction_terms']) > 1) { ?>

                                    <p><?php echo $CORE->_e(array('auction', '65')); ?></p>

                                    <div >
                                              VOTRE ARGENT EST EN SÉCURITÉ : Le vendeur ne pourra recevoir la montant de la vente tant que la journée de retrait prévu du bien ne passe et vous que ne rentriez en possession de votre achat.
											 <br>
										<img style="width:140px; margin-top: 10px;" src = "<?php echo $_SERVER['REQUEST_URI'].'../../wp-content/themes/AT/framework/class/images/secure.png' ?>" >
									
                                    </div>
									

                                <?php } ?>

                                <hr/>
  	<!-- Resolution du probleme de l'enchère direct (Achat direct) : mise en place du bouton d'achat
                                <form method="post" action="" name="buynowform" id="buynowform" class="article-form">
                                    <input type="hidden" name="auction_action" value="buynow"/>

                                    <div class="col-md-12" style="margin: 0px 0 20px;">
                                        <div class="checkbox" style="padding-left:10px;">
                                            <input type="checkbox" name="byitnow" class="radio terms">
                                            J'ai lu et j'accepte <a
                                                    href="<?php //echo $GLOBALS['CORE_THEME']['links']['terms']; ?>"
                                                    style="color:blue; text-decoration:underline;" target="_blank">les
                                                termes et conditions</a>
                                        </div>

                                    </div>
                                    <div class="text-center">
                                        <button id="btn-buy-now" class="btn btn-lg btn-primary" type="submit"
                                                disabled="disabled"><?php //echo $CORE->_e(array('auction', '66')); ?></button>
                                    </div>
                                </form> 
								-->
								<!-- Resolution du probleme de l'enchère direct (Achat direct) : mise en place du bouton d'achat-->
				<button style="width: 45%" onclick="gettotal()" id="btn_payment" href="#myShippingAdress" role="button" class="btn btn-info btn-lg"
                        data-toggle="modal"><?php echo $CORE->_e(array('button', '22')); ?>
				</button>
<!--  get latest stripe and paypal payment forms  if sa is already valide -->
										
					<script>
									function is_sa_already_valide(){
										// alert(jQuery('#sa_txt_valide').html());
										if( jQuery('#sa_txt_valide').html() != "" ) {
											jQuery('#total_price').html(total);
										}
									}
					</script>
                <div id="myShippingAdress" style="text-align:center display:inline; width:100%; margin-left: 4%" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myShippingAdress"  aria-hidden="true">
					 
<!-- SHIPPING ADDRESS -->

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  font-weight: normal;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  font-weight: normal;
}

::-ms-input-placeholder { /* Microsoft Edge */
  font-weight: normal;
}
label {
    font-weight: normal;
}

.sa_select{
	height: 38px;
    width: 100%;
    margin-bottom: 20px;
    padding: 10px;
    border: none;
    font-weight: 700;
    background: #f2f2f2;
    border-bottom: solid;
    border-bottom-width: 1px;
	outline: none;	
    font-size: 16px;
}

.sa_body {
    font-family: Arial;
    font-size: 17px;
    padding: 8px;
    width: 46%;
    margin-left: 10%;
}

* {
  box-sizing: border-box;
}

.sa_row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 0% 2%;
	float: left;
	text-align: center;
	background-color: #f2f2f2;
}

.sa_margin{
	margin-top: 4%;
}

.modal-dialog {
    padding-top: 4%;
    margin-left: 54%;
}

.sa_col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.sa_col-50 {
  -ms-flex: 50%; /* IE10 */
   flex: 52%;
}

.sa_col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.sa_col-25,
.sa_col-50,
.sa_col-75 {
/* padding: 0 16px; */
  
}

.sa_container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border-radius: 3px;
}

.sa_input {
	width: 100%;
    height: 38px;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 3px;
    font-weight: 550;
    border: none;
    background: #f2f2f2;
    border-bottom: solid;
    border-bottom-width: 1px;
    outline: none;
    font-size: 16px;
    margin-bottom: 25px;
}

sa_label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

.sa_btn {
  background-color: #4477bd;
}

.sa_btn:hover {
  background-color: #999;
}


hr {
  border: 1px solid lightgrey;
}

span.price {
  float: right;
  color: grey;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 800px) {
  .sa_row {
    flex-direction: column-reverse;
  }
  .sa_col-25 {
    margin-bottom: 20px;
  }
}
</style>

<style>
.error {
  color: red;
  margin-left: 5px;
}
 
label.error {
  display: inline;
}


</style>
<!-- country list for shipping info form --> 
<script src="<?php echo get_home_url()."/wp-content/themes/AT/framework/class/countries12/js/country_fr.js"; ?>"></script>

<script>
// load a list of province if country have it, or if provinve is clicked and coundtry have it, otherwise a textbox with thr province
var code_with_province = ['CM', 'CA', 'US']; 
jQuery("#core_padding").on('change', '#state', function ( e ) {
	var code = jQuery( "option:selected", "#state" ).val();
	if( jQuery.inArray( code, code_with_province ) !== -1 ){
		var options = "<select  name='sa_province' id='province' class='sa_select'>"+ getProvinceOptionsHtml( code ) +"</select>";
		jQuery( "#province_container" ).html( options ); // select box...
	}
	else{
		jQuery( "#province_container" ).html( "<input id='province' class='sa_province_txt_input sa_input' type='text' placeholder='Province ( pas obligatoire )' value=''>" ); // ... or text input
	}
});
// quand on click sur cette zone de txt on peut afficher les provincve si le pays en a
jQuery("#core_padding").on('click', '.sa_province_txt_input', function ( e ) {
	var code = jQuery( "option:selected", "#state" ).val(); // click on text input
	if( jQuery.inArray( code, code_with_province ) !== -1 ){ // jquery in array
		var options = "<select  name='sa_province' id='province' class='sa_select'>"+ getProvinceOptionsHtml( code ) +"</select>";
		jQuery( "#province_container" ).html( options ); // select box
	}

});

// get a country provinces
var province_default = "<?php echo $sa_province ?>"; // default user provinc

function getProvinceOptionsHtml( code ){
	if( code == "CA" ){
		var provinces = [ 'Alberta', 'Colombie-Britannique', 'Manitoba', 'Nouveau-Brunswick', 'Terre-Neuve-et-Labrador', 'Nouvelle-Écosse', 'Nunavut', 'Territoires du Nord-Ouest', 'Ontario', 'Île-du-Prince-Édouard', 'Québec ',' Saskatchewan ','Yukon' ];
	}
	else if( code == "CM" ){
		var provinces = [ 'Extrême-Nord', 'Nord', 'Adamaoua', 'Est', 'Centre', 'Sud', 'Littoral', 'Ouest', 'Nord-Ouest', 'Sud-Ouest' ];
	}
	else if ( code == "US" ){
		var provinces = ['Alabama', 'Arizona', 'Arkansas', 'Californie', 'Colorado', 'Connecticut', 'Delaware', 'District de Columbia', 'Floride', 'Géorgie', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiane', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana ', 'Nebraska ', 'Nevada ', 'New Hampshire' ,'New Jersey ', 'Nouveau Mexique ', 'New York ', 'Caroline du Nord ', 'Dakota du Nord ', 'Ohio ', 'Oklahoma ', 'Oregon ', 'Pennsylvanie ', 'Rhode Island ', 'Caroline du Sud ', 'Dakota du Sud ', 'Tennessee ', 'Texas ', 'Utah ', 'Vermont ', 'Virginie ', 'Washington ', 'Virginie occidentale ', 'Wisconsin ', 'Wyoming ', 'Porto Rico ', 'Îles Vierges ', 'Îles Mariannes du Nord ', 'Guam ', 'Samoa américaines ', 'Palaos'];
	}
	var optionsHtml = "";
	for( var i = 0; i < provinces.length; i++  ){
		if( province_default ==  provinces[i] ){
			optionsHtml += "<option selected =" + province_default +" >" + provinces[i] + "</option>";
		}
		else{
			optionsHtml += "<option>" + provinces[i] + "</option>";
		}
	}

	// console.log( optionsHtml );
	return optionsHtml;
}

var userid  = <?php echo $userid ?>;
var val_img = "<?php echo get_template_directory_uri() . '/framework/class/images/valide.png' ?>"
// validate shipping adress datas on click
jQuery("#core_padding").on('click', '#sa_btn', function ( e ) {
    // e.preventDefault();

	// jQuery(this).off("click").attr('href', '\#');
	
	
	// petit config
	
    var fname     = jQuery( "#fname" ).val();
    var adr       = jQuery ("#adr" ).val();
    var city      = jQuery( "#city" ).val();
	var province  = jQuery( "#province" ).val(); // can be a choice list or a text input value
    var state     = jQuery( "#state" ).val();
    var zip       = jQuery( "#zip" ).val();
    var email     = jQuery( "#email" ).val();
	var regEx     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	var validEmail = regEx.test(email);
	
    jQuery(".error").remove();
 
    if (fname.length < 1) {
      jQuery('#fname').before('<span class="error">Entrez votre nom complet</span>');
    }
    else if (email.length < 1) {
      jQuery('#email').before('<span class="error">Entrez votre adresse email</span>');
    } 
    else if (!validEmail) {
        jQuery('#email').before('<span class="error">Entrez une adresse amail valide</span>');
    }
    else if (adr.length < 1) {
      jQuery('#adr').before('<span class="error">Entrez votre adresse de livraison</span>');
    }
    else if (city.length < 1) {
      jQuery('#city').before('<span class="error">Entrez votre ville</span>');
    }
    else if (state.length < 1) {
      jQuery('#state').before('<span class="error">Entrez votre pays</span>');
    }
    else if (zip.length < 1) {
      jQuery('#zip').before('<span class="error">Entrez votre code zip</span>');
    }
	else { 
		// load latest paiement form
		jQuery('#total_price').html(total);
		// insert or update user shipping address
		function jsonServerResponse(operation, callback, JSOoptionalData) {
			JSOoptionalData = (typeof JSOoptionalData == 'undefined') ? 'defaultValue' : JSOoptionalData
			jQuery.ajax({
				type: 'GET',
				url:  '".str_replace("https://", "", str_replace("http://", "", get_home_url())) . "/?core_aj=1&action=sa_update&userid='+userid+'&sa_fname='+fname+'&sa_adr='+adr+'&sa_city='+city+'&sa_state='+state+'&sa_province='+province+'&sa_zip='+zip+'&sa_email='+email+'',
				success: function( data ){
					console.log('res: ' + data);
				}
			});	
		}
		jsonServerResponse('get_something_from_server', function(returnedJSO){	console.log(returnedJSO.Result.Some_data);}, 'optional_data');

	
		// validation succes txt
		jQuery('#sa_txt_valide').html("Adresse de livraison validée");
		
		// validation succes img
		jQuery('#sa_img_valide').html("<img src='"+val_img+"'style='width:30px;'>");
	}
  })


</script>
 <!-- Shipping adress box -->
<div id="sa_container" class="sa_row sa_body sa_margin">
  <div class="sa_col-75">
    <div class="sa_container">
        <div class="sa_row">
          <div class="sa_col-50">
<?php
//get shipping adress infos if any
$sa_fname     = get_user_meta( $userdata->ID, 'sa_fname', true);
$sa_adr       = get_user_meta( $userdata->ID, 'sa_adr', true);
$sa_city      = get_user_meta( $userdata->ID, 'sa_city', true);
$sa_state     = get_user_meta( $userdata->ID, 'sa_state', true);
$sa_province  = get_user_meta( $userdata->ID, 'sa_province', true);
$sa_zip       = get_user_meta( $userdata->ID, 'sa_zip', true);
$sa_email     = get_user_meta( $userdata->ID, 'sa_email', true);
$cdef = ( $sa_state ) ? $sa_state : "Canada"; // default country code
// echo $cdef
?>

            <h3><center style="margin-bottom: 35px;">Livrer à</center></h3>

            <input class="sa_input" type="text" id="fname" name="firstname" placeholder="Nom complet" value="<?php echo $sa_fname ?>" >
            <input class="sa_input" type="text" id="email" name="email" placeholder="Email" value="<?php echo $sa_email ?>">
            <input class="sa_input" type="text" id="adr" name="address" placeholder="Adresse de livraison" value="<?php echo $sa_adr ?>">
            <input class="sa_input" type="text" id="city" name="city" placeholder="Ville" value="<?php echo $sa_city ?>" >
<label for="state">Pays ou territoir</label>
				<select id="state" name="state" class="sa_select country_list" defvalue="<?php echo $cdef ?>" >
				</select> 

            <div class="sa_row">
              <div class="sa_col-50">
				<span id="province_container">
                <input class="sa_province_txt_input sa_input" type="text" id="province" name="province" placeholder="Province ( pas obligatoire )" value="<?php echo $sa_province ?>" >
				</span>
              </div>

              <div class="sa_col-50">
                <input class="sa_input" type="text" id="zip" name="zip" placeholder="Zip / boite postale" value="<?php echo $sa_zip ?>" >
              </div>
            </div>
          </div>
		</div>
	</div>
          <br>
		<button id="sa_btn" role="button" class="btn btn-info" style="cursor:pointer"  >
			Confirmez votre adresse de livraison
		</button>
		<br><br>
		<span id="sa_txt_valide"></span>
		<br>
		<span id="sa_img_valide" style="color:#888"></span>
		<br><br>
    </div>

  </div>
  <!-- myPaymentOptions box -->
  <div style="float:left; width: 37%; margin-top: 3.8%">
  	<div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h4 id="myModalLabel"><?php echo $CORE->_e(array('single', '13')); ?>
                                    (<?php 
									$total = ( get_post_meta($post->ID, 'tmp_price', true) ? get_post_meta($post->ID, 'tmp_price', true): get_post_meta($post->ID, 'price_bin', true) );
									echo hook_price( $total + $price_shipping); ?> )</h4>
                            </div>
<div class="modal-body" id="total_price"><div class="row-old"> 
				   <div class="col-md-8"><b>Payer Maintenant Avec Paypal</b></div>
				   <div class="col-md-4"><button onclick="alert('Veuillez d\'abord valider le formulaire de livraison svp')"  class="stripe-button-el" style="width: 98px; background: #e2e2e2; visibility: visible;"><span style="display: block; min-height: 30px;">Payer</span></button></div>
				   </div>
				   <div class="clearfix"></div><hr>


<div class="row-old">
					   <div class="col-md-8"><b>Payer Maintenant par carte via Stripe. Le moyen le plus sécuritaire de faire des transactions en ligne.</b></div>
					   <div class="col-md-4">
					   
<button onclick="alert('Veuillez d\'abord valider le formulaire de livraison svp')"  class="stripe-button-el" style="background: #e2e2e2; visibility: visible;"><span style="display: block; min-height: 30px;">Pay with Card</span></button>	   
	
	</div> <div class="clearfix"></div></div><div class="clearfix"></div>
</div>
                            <div class="modal-footer">
                                <?php echo $CORE->admin_test_checkout(); ?>
                                <button class="btn" data-dismiss="modal"
                                        aria-hidden="true"><?php echo $CORE->_e(array('single', '14')); ?>
								
								</button>
                            </div>
                        </div>
                   
  </div>
  
</div>


<!-- END INLINE SHIPPING INFO & PAYMENT BOX -->			



                            </li>
                        <?php } ?>


                    <?php } // END BIDDING AREA
                    ?>


                </ul>
            </div>
        </div>

        <?php
        // RETURN OUTPUT
        $output = ob_get_contents();
        ob_end_clean();

        return $output;

    }

    function shortcode_bids()
    {
        global $CORE, $post, $wpdb;

        $bidding_history = get_post_meta($post->ID, 'current_bid_data', true);
        if (is_array($bidding_history) && !empty($bidding_history)) {
            return count($bidding_history);
        } else {
            return 0;
        }

    }

    function shortcode_biddinghistory()
    {
        global $CORE, $post, $wpdb;

        $bidding_history = get_post_meta($post->ID, 'current_bid_data', true);

        // START OUTPUT
        ob_start();

        // LOOP LIST
        if (is_array($bidding_history) && !empty($bidding_history)) {

            $bidding_history = $CORE->multisort($bidding_history, array('max_amount'));

            // BUILD DATA
            ?>
            <ul class="list-group" <?php if (is_admin() && count($bidding_history) > 5) { ?> style="max-height:400px; overflow:scroll; "<?php } ?>>

                <?php foreach ($bidding_history as $kk => $bhistory) { ?>

                    <li class="list-group-item">


                        <?php if (is_admin()) { ?>

                            User: <a
                                    href="<?php echo get_author_posts_url($bhistory['userid']); ?>"><?php echo $bhistory['username']; ?></a>
                            <br/>

                            Max Bid: <?php echo hook_price($bhistory['max_amount']); ?> <br/>

                            Date:  <?php echo hook_date($bhistory['date']); ?>

                            <hr/>

                        <?php } else { ?>

                            <span class="badge pull-right"><small><?php echo hook_date($bhistory['date']); ?></small></span>

                            <small>
                                <a href="<?php echo get_author_posts_url($bhistory['userid']); ?>"><?php echo $bhistory['username']; ?></a>
                            </small>

                        <?php } ?>

                    </li>
                <?php } ?>
            </ul>

        <?php } else { ?>

            <div class="text-center"><h4><?php echo $CORE->_e(array('auction', '87')); ?></h4></div>

        <?php }

        // RETURN OUTPUT
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }


    function _hook_language_array($c)
    {

        $d = array(

            "1" => __("Item not sold.", "premiumpress"),
            "2" => __(" was the winning bidder.", "premiumpress"),
            "3" => __("Auction Finished", "premiumpress"),
            "4" => __("Type", "premiumpress"),
            "5" => __("Normal Auction", "premiumpress"),
            "6" => __("Classifieds (Buy Now Only)", "premiumpress"),
            "7" => __("Here you can choose the format of your auction.", "premiumpress"),
            "8" => __("Buy Now Price", "premiumpress"),
            "9" => __("Here you can set a price for the user to buy this item outright.", "premiumpress"),
            "10" => __("Reserve Price", "premiumpress"),
            "11" => __("Here you can set the lowest price your willing to sell this item for.", "premiumpress"),
            "12" => __("Auction Length", "premiumpress"),
            "13" => __("Select the number of days you would like the auction to run for.", "premiumpress"),
            "14" => __("My Payment Options", "premiumpress"),
            "15" => __("Paypal email updated successfully.", "premiumpress"),
            "16" => __("Please enter your PayPal email below, all of your auction payments will be sent to this email minus our service and commission charges.", "premiumpress"),
            "17" => __("PayPal Email", "premiumpress"),
            "18" => __("Bidding History", "premiumpress"),
            "19" => __("Item", "premiumpress"),
            "20" => __("Max Bid", "premiumpress"),
            "21" => __("Actions", "premiumpress"),
            "22" => __("Auction Status", "premiumpress"),
            "23" => __("Leave Feedback", "premiumpress"),
            "24" => __("View Auction", "premiumpress"),
            "25" => __("Feedback you left on", "premiumpress"),
            "26" => __("Here you can setup your payment options for receiving payments.", "premiumpress"),
            "27" => __("Here you can view your bidding history.", "premiumpress"),
            "28" => __("You are the winning bidder.", "premiumpress"),
            "29" => __("Bid amount of (%a) is invalid.", "premiumpress"),
            "30" => __("You have been outbid!", "premiumpress"),
            "31" => __("You are now the highest bidder.", "premiumpress"),
            "32" => __("Highest", "premiumpress"),
            "33" => __("Your max bid is", "premiumpress"),
            "34" => __("and the sellers reserve price is", "premiumpress"),
            "35" => __("<b>Note</b> If this auction ends your bid will be increased to match the reserve.", "premiumpress"),
            "36" => __("which is less than the sellers reserve price of", "premiumpress"),
            "37" => __("<b>Note</b> If the reserve price is not met the item will not be sold.", "premiumpress"),
            "38" => __("Reserve Price Not Met", "premiumpress"),
            "39" => __("This auction has ended.", "premiumpress"),
            "40" => __("was the winning bidder.", "premiumpress"),
            "41" => __("Contact Seller", "premiumpress"),
            "42" => __("Contact Buyer", "premiumpress"),
            "43" => __("Please leave some feedback about this transaction.", "premiumpress"),
            "44" => __("Please leave a user rating:", "premiumpress"),
            "45" => __("Bad (0%)", "premiumpress"),
            "46" => __("Poor", "premiumpress"),
            "47" => __("Regular", "premiumpress"),
            "48" => __("Good", "premiumpress"),
            "49" => __("Excellent (100%)", "premiumpress"),
            "50" => __("Submit", "premiumpress"),
            "51" => __("Bidding Options", "premiumpress"),
            "52" => __("What would you like to do?", "premiumpress"),
            "53" => __("Bid", "premiumpress"),
            "54" => __("You cannot bid on your own item.", "premiumpress"),
            "55" => __("Buy Now", "premiumpress"),
            "56" => __("Please login to bid.", "premiumpress"),
            "57" => __("Reserve price has not yet met.", "premiumpress"),
            "58" => __("Contact seller", "premiumpress"),
            "59" => __("Positive Feedback", "premiumpress"),
            "60" => __("View other items by this seller", "premiumpress"),
            "61" => __("Joined", "premiumpress"),
            "62" => __("Confirm Bidding", "premiumpress"),
            "63" => __("Please check and confirm your bid amount.", "premiumpress"),
            "64" => __("Bid Amount:", "premiumpress"),
            "65" => __("By making a bid you confirm to our website terms and conditions", "premiumpress"),
            "66" => __("Confirm Bid", "premiumpress"),
            "67" => __("Shipping Price", "premiumpress"),
            "68" => __("Here you enter an amount for shipping this item.", "premiumpress"),
            "69" => __("Reserve price met.", "premiumpress"),
            "70" => __("Make Bid", "premiumpress"),
            "71" => __("Seller", "premiumpress"),
            "72" => __("You cannot bid on your own auctions.", "premiumpress"),
            "73" => __("Please bid greater than ", "premiumpress"),
            "74" => __("Starting Price", "premiumpress"),
            "75" => __("This is the price the bidding will start at.", "premiumpress"),
            "76" => __("finished", "premiumpress"),
            "77" => __("active", "premiumpress"),
            "78" => __("now", "premiumpress"),
            "79" => __("<b>Now What?</b><br />The user has not specified a payment method. Please contact the user for payment instructions.", "premiumpress"),
            "80" => __("Would you like to re-list this item?", "premiumpress"),
            "81" => __("You can re-list this item free for %a days.", "premiumpress"),
            "82" => __("Re-list Item", "premiumpress"),
            "83" => __("Bidding ends in;", "premiumpress"),
            "84" => __("Current Price", "premiumpress"),
            "85" => __("Sellers Details", "premiumpress"),
            "86" => __("Bidding History", "premiumpress"),
            "87" => __("No Bidding History", "premiumpress"),
            "88" => __("bids", "premiumpress"),
            "89" => __("Enter your <b>max</b> bid", "premiumpress"),
            "90" => __("<p>To help you get the best price possible we have an automated bidding system. </p><p>Enter the maximum amount you are willing to pay for this item and our system will start at the lowest bid price and automatically re-bid for you up to your maximum bid.</p><p>This way you do not need to bid again and will always get the item for the best possible price.</p>
	", "premiumpress"),
            "91" => __("Condition", "premiumpress"),
            "92" => __("New", "premiumpress"),
            "93" => __("Used", "premiumpress"),
            "94" => __("available", "premiumpress"),
            "95" => __("Buy Now Quantity", "premiumpress"),
            "96" => __("Enter the number of items available for sale.", "premiumpress"),
            "97" => __("Bids", "premiumpress"),
            "98" => __("Won", "premiumpress"),
            "99" => __("Messages", "premiumpress"),
			"100" => __("Votre montant est trop bas pour une enchère automatique. Nous vous recommandons d'inscrire un montant plus élevé. Votre mise sera considerée comme une enchère fixe.", "premiumpress")
        );

        $c['english']['auction'] = $d;

        return $c;

    }


    function actions_auctionended()
    {
        global $post, $userdata, $CORE;

        // GET CURRENT BIDING DATA
        $current_bidding_data = get_post_meta($post->ID, 'current_bid_data', true);
        if (!is_array($current_bidding_data)) {
            $current_bidding_data = array();
        }

        //2. ORDER IT BY KEY (WHICH HOLDS THE BID AMOUNT)
        krsort($current_bidding_data);

        //3. GET THE CURRENT BIDDING DATA
        $checkme = current($current_bidding_data);

        // 4.PRICE DATA

        $current_priceaugmo = get_post_meta($post->ID, 'ket_pourcentageaugmentation', true);

        $current_price = get_post_meta($post->ID, 'price_current', true);
        if ($current_price == "" || !is_numeric($current_price)) {
            $current_price = 0;
        }
        $reserve_price = get_post_meta($post->ID, 'price_reserve', true);
        if ($reserve_price == "" || !is_numeric($reserve_price)) {
            $reserve_price = 0;
        }
        $price_shipping = get_post_meta($post->ID, 'price_shipping', true);
        if ($price_shipping == "" || !is_numeric($price_shipping)) {
            $price_shipping = 0;
        }

        // RE-LIST BUTTON
        if ($post->post_author == $userdata->ID && isset($GLOBALS['CORE_THEME']['auction_relist']) && $GLOBALS['CORE_THEME']['auction_relist'] == '1') {

            $days_renew = get_option($post->ID, 'listing_expiry_days', true);

            // GET DAYS FROM THE PACKAGE
            if (!is_numeric($days_renew)) {
                $packageID = get_post_meta($post->ID, 'packageID', true);
                $packagefields = get_option("packagefields");
                if (isset($packagefields[$packageID]['expires']) && is_numeric($packagefields[$packageID]['expires'])) {
                    $days_renew = $packagefields[$packageID]['expires'];
                }
            }

            if (!is_numeric($days_renew)) {
                $days_renew = 30;
            }

            ?>

            <h4><?php echo $CORE->_e(array('auction', '80')); ?></h4>

<!-- part 3 -->		
<?php /**
$c_sticker = get_post_meta( $post->ID, 'listing_sticker',true );
if(  in_array( 'administrator', (array) $userdata->roles ) || in_array('editor', (array) $userdata->roles )){
			// The user has the "administrator" or editor role
			$role = 1;
} 

if( $role == 1 && $c_sticker == 11 ){
	echo "";
}
else{
	echo "<p>".str_replace('%a', $days_renew, $CORE->_e(array('auction', '81')))."</p>";
}
**/
?>
<!-- fin part 3 -->	

			<p><?php echo str_replace("%a", $days_renew, $CORE->_e(array('auction', '81'))); ?></p>
		
            <a href="<?php echo get_permalink($post->ID); ?>?relistme=1"
               class="btn btn-success"><?php echo $CORE->_e(array('auction', '82')); ?></a>

            <hr/>

            <?php

        }


        // CHECK IF IM THE WINNING BIDDER THEN DISPLAY PAYMENT BUTTONS
        if ($checkme['userid'] == $userdata->ID && $current_price >= $reserve_price && (get_post_meta($post->ID, 'auction_price_paid', true) == "")) {

            // IF THE SELLER IS THE ADMIN, THEN ACCEPT ANY ADMIN PAMENT
            if (user_can($post->post_author, 'administrator')) {

                ?>

                <button href="#myPaymentOptions" role="button" class="btn btn-info btn-lg"
                        data-toggle="modal"><?php echo $CORE->_e(array('button', '22')); ?></button>

                <div id="myPaymentOptions" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h4 id="myModalLabel"><?php echo $CORE->_e(array('single', '13')); ?>
                                    (<?php echo get_post_meta( $postid, 'tmp_price', $price, true ) ?  hook_price( get_post_meta( $postid, 'tmp_price', $price, true )+ $price_shipping ) :  hook_price( $current_price + $price_shipping ); ?>)</h4>
                            </div>
                            <div class="modal-body"><?php echo $CORE->PAYMENTS($current_price + $price_shipping, "CART-" . $post->ID . "-" . $userdata->ID . "-" . date("Ymdi"), $post->post_title, $post->ID, $subscription = false); ?></div>
                            <div class="modal-footer">

                                <?php echo $CORE->admin_test_checkout(); ?>

                                <button class="btn" data-dismiss="modal"
                                        aria-hidden="true"><?php echo $CORE->_e(array('single', '14')); ?></button>

                            </div>
                        </div>
                    </div>
                </div>


            <?php } else {


                // CHECK IF THIS IS A USER AUCTION AND THEY HAVE SET A PAYPAL EMAIL
                $paypalemail = get_user_meta($post->post_author, 'user_paypalemail', true);

                if ($paypalemail == "") {

                    ?>

                    <br/>
                    <div class="well">
                        <?php echo $CORE->_e(array('auction', '79')); ?>
                    </div>

                    <?php

                } else {

                    $GLOBALS['A_TOTAL'] = $current_price + $price_shipping;
                    $GLOBALS['A_ORDERID'] = 'USERPAYMENT-' . $post->ID . '-' . date('Ydm');
                    $GLOBALS['A_DESC'] = strip_tags($post->post_title);

                    echo hook_custom_paypal_payment('<form method="post"  action="https://www.paypal.com/cgi-bin/webscr" name="checkout_paypal" class="pull-left">	 
					<input type="hidden" name="lc" value="US">
					<input type="hidden" name="return" value="' . $GLOBALS['CORE_THEME']['links']['callback'] . '/?status=thankyou">
					<input type="hidden" name="cancel_return" value="' . $GLOBALS['CORE_THEME']['links']['callback'] . '">
					<input type="hidden" name="notify_url" value="' . $GLOBALS['CORE_THEME']['links']['callback'] . '">
					<input type="hidden" name="discount_amount_cart" value="0">
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="amount" value="' . $GLOBALS['A_TOTAL'] . '">
					<input type="hidden" name="item_name" value="' . $GLOBALS['A_DESC'] . '">
					<input type="hidden" name="item_number" value="' . $GLOBALS['A_ORDERID'] . '">
					<input type="hidden" name="business" value="' . $paypalemail . '">
					<input type="hidden" name="currency_code" value="' . $GLOBALS['CORE_THEME']['currency']['code'] . '">
					<input type="hidden" name="charset" value="utf-8">
					<input type="hidden" name="custom" value="' . $GLOBALS['A_ORDERID'] . '">
					<button  class="btn btn-lg btn-info">' . $CORE->_e(array('button', '21')) . '</button>					
					</form>');

                }

            }

            // CONTACT SELLER & BUYER BUTTONS
            if (isset($GLOBALS['CORE_THEME']['message_system']) && $GLOBALS['CORE_THEME']['message_system'] != '0') {

                if ($userdata->ID == $checkme['userid']) {
                    ?>

                    <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount']; ?>/?u=<?php echo get_the_author_meta('user_login', $post->post_author); ?>&tab=msg&show=1"
                       class="btn btn-info btn-lg">
                        <?php echo $CORE->_e(array('auction', '41')); ?>
                    </a>


                    <?php

                } else {
                    ?>

                    <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount']; ?>/?u=<?php echo $checkme['username']; ?>&tab=msg&show=1"
                       class="btn btn-info btn-lg">
                        <?php echo $CORE->_e(array('auction', '42')); ?>
                    </a>


                    <?php
                }

            }

            if ($checkme['userid'] == $userdata->ID && $CORE->FEEDBACKEXISTS($post->ID, $userdata->ID) === false) {

                ?>

                <div class="clearfix"></div>
                <hr>

                <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount']; ?>/?fdid=<?php echo $post->ID; ?>"
                   class="btn btn-success">
                    <?php echo $CORE->_e(array('auction', '23')); ?>
                </a>

                <?php
            }


        } elseif ($checkme['userid'] == $userdata->ID && get_post_meta($post->ID, 'auction_price_paid', true) != "") {

            // LEAVE FEEDBACK
            if ($CORE->FEEDBACKEXISTS($post->ID, $userdata->ID) == false) {

                ?>

                <a href="<?php echo $GLOBALS['CORE_THEME']['links']['myaccount']; ?>/?fdid=<?php echo $post->ID; ?>"
                   class='btn btn-lg btn-info'>
                    <?php echo $CORE->_e(array('feedback', '1')); ?>
                </a>

                <?php

            }
        }


    }


}

?>

